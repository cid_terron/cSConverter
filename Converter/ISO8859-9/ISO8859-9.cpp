//Automatically created by Utility Tool

#include "cSConverter/ISO8859-9.h"
#include "cSConverter/UTF8.h"

#include "cSConverter/Alloc_Fkt.h"

#include "string.h"

namespace cSConverter
{
ISO8859_9::ISO8859_9() : ISO8859(SCONV_E_ISO8859_9)
{
}

ISO8859_9::ISO8859_9(const ISO8859_9 &other) : ISO8859(SCONV_E_ISO8859_9)
{
   assign(other);
}

ISO8859_9::ISO8859_9(const UTF_Common &other) : ISO8859(SCONV_E_ISO8859_9)
{
   assign(other);
}

UTF_Common &ISO8859_9::operator=(ISO8859_9 &other)
{
   uint32_t size = other.get_string_size();
   m_string = CSCONVERTER_NEW(char[size+1]);
   memcpy(m_string, other.get_string(), size);
   m_string[size] = 0;

   return *this;
}

UTF_Common &ISO8859_9::operator=(UTF_Common &other)
{
   return assign(other);
}

UTF_Common &ISO8859_9::operator+=(ISO8859_9 &other)
{
   uint32_t size = other.get_string_size();
   char *tmp = CSCONVERTER_NEW(char[size + m_size+1]);

   memcpy(tmp, m_string, m_size);
   memcpy(&tmp[m_size], other.get_string(), size);
   m_size += size;
   CSCONVERTER_DELETE2(m_string, char);
   m_string = tmp;
   m_string[m_size] = 0;

   return *this;
}

UTF_Common &ISO8859_9::operator+=(UTF_Common &other)
{
   return append(other);
}

UTF_Common &ISO8859_9::operator+=(const char *str)
{
   add_cstring(str);
   return *this;
}

UTF_Common &ISO8859_9::operator=(const char *str)
{
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
      m_string = 0;
      m_size = 0;
   }

   add_cstring(str);

   return *this;
}

uint32_t ISO8859_9::get_next()
{
   uint32_t ret = 0;
   ret = m_string[m_pos];
   m_pos++; 
   switch(ret)
   {
      case 0x0:
      {
         ret = 0x0;
         break;
      }
      case 0x1:
      {
         ret = 0x1;
         break;
      }
      case 0x2:
      {
         ret = 0x2;
         break;
      }
      case 0x3:
      {
         ret = 0x3;
         break;
      }
      case 0x4:
      {
         ret = 0x4;
         break;
      }
      case 0x5:
      {
         ret = 0x5;
         break;
      }
      case 0x6:
      {
         ret = 0x6;
         break;
      }
      case 0x7:
      {
         ret = 0x7;
         break;
      }
      case 0x8:
      {
         ret = 0x8;
         break;
      }
      case 0x9:
      {
         ret = 0x9;
         break;
      }
      case 0xa:
      {
         ret = 0xa;
         break;
      }
      case 0xb:
      {
         ret = 0xb;
         break;
      }
      case 0xc:
      {
         ret = 0xc;
         break;
      }
      case 0xd:
      {
         ret = 0xd;
         break;
      }
      case 0xe:
      {
         ret = 0xe;
         break;
      }
      case 0xf:
      {
         ret = 0xf;
         break;
      }
      case 0x10:
      {
         ret = 0x10;
         break;
      }
      case 0x11:
      {
         ret = 0x11;
         break;
      }
      case 0x12:
      {
         ret = 0x12;
         break;
      }
      case 0x13:
      {
         ret = 0x13;
         break;
      }
      case 0x14:
      {
         ret = 0x14;
         break;
      }
      case 0x15:
      {
         ret = 0x15;
         break;
      }
      case 0x16:
      {
         ret = 0x16;
         break;
      }
      case 0x17:
      {
         ret = 0x17;
         break;
      }
      case 0x18:
      {
         ret = 0x18;
         break;
      }
      case 0x19:
      {
         ret = 0x19;
         break;
      }
      case 0x1a:
      {
         ret = 0x1a;
         break;
      }
      case 0x1b:
      {
         ret = 0x1b;
         break;
      }
      case 0x1c:
      {
         ret = 0x1c;
         break;
      }
      case 0x1d:
      {
         ret = 0x1d;
         break;
      }
      case 0x1e:
      {
         ret = 0x1e;
         break;
      }
      case 0x1f:
      {
         ret = 0x1f;
         break;
      }
      case 0x20:
      {
         ret = 0x20;
         break;
      }
      case 0x21:
      {
         ret = 0x21;
         break;
      }
      case 0x22:
      {
         ret = 0x22;
         break;
      }
      case 0x23:
      {
         ret = 0x23;
         break;
      }
      case 0x24:
      {
         ret = 0x24;
         break;
      }
      case 0x25:
      {
         ret = 0x25;
         break;
      }
      case 0x26:
      {
         ret = 0x26;
         break;
      }
      case 0x27:
      {
         ret = 0x27;
         break;
      }
      case 0x28:
      {
         ret = 0x28;
         break;
      }
      case 0x29:
      {
         ret = 0x29;
         break;
      }
      case 0x2a:
      {
         ret = 0x2a;
         break;
      }
      case 0x2b:
      {
         ret = 0x2b;
         break;
      }
      case 0x2c:
      {
         ret = 0x2c;
         break;
      }
      case 0x2d:
      {
         ret = 0x2d;
         break;
      }
      case 0x2e:
      {
         ret = 0x2e;
         break;
      }
      case 0x2f:
      {
         ret = 0x2f;
         break;
      }
      case 0x30:
      {
         ret = 0x30;
         break;
      }
      case 0x31:
      {
         ret = 0x31;
         break;
      }
      case 0x32:
      {
         ret = 0x32;
         break;
      }
      case 0x33:
      {
         ret = 0x33;
         break;
      }
      case 0x34:
      {
         ret = 0x34;
         break;
      }
      case 0x35:
      {
         ret = 0x35;
         break;
      }
      case 0x36:
      {
         ret = 0x36;
         break;
      }
      case 0x37:
      {
         ret = 0x37;
         break;
      }
      case 0x38:
      {
         ret = 0x38;
         break;
      }
      case 0x39:
      {
         ret = 0x39;
         break;
      }
      case 0x3a:
      {
         ret = 0x3a;
         break;
      }
      case 0x3b:
      {
         ret = 0x3b;
         break;
      }
      case 0x3c:
      {
         ret = 0x3c;
         break;
      }
      case 0x3d:
      {
         ret = 0x3d;
         break;
      }
      case 0x3e:
      {
         ret = 0x3e;
         break;
      }
      case 0x3f:
      {
         ret = 0x3f;
         break;
      }
      case 0x40:
      {
         ret = 0x40;
         break;
      }
      case 0x41:
      {
         ret = 0x41;
         break;
      }
      case 0x42:
      {
         ret = 0x42;
         break;
      }
      case 0x43:
      {
         ret = 0x43;
         break;
      }
      case 0x44:
      {
         ret = 0x44;
         break;
      }
      case 0x45:
      {
         ret = 0x45;
         break;
      }
      case 0x46:
      {
         ret = 0x46;
         break;
      }
      case 0x47:
      {
         ret = 0x47;
         break;
      }
      case 0x48:
      {
         ret = 0x48;
         break;
      }
      case 0x49:
      {
         ret = 0x49;
         break;
      }
      case 0x4a:
      {
         ret = 0x4a;
         break;
      }
      case 0x4b:
      {
         ret = 0x4b;
         break;
      }
      case 0x4c:
      {
         ret = 0x4c;
         break;
      }
      case 0x4d:
      {
         ret = 0x4d;
         break;
      }
      case 0x4e:
      {
         ret = 0x4e;
         break;
      }
      case 0x4f:
      {
         ret = 0x4f;
         break;
      }
      case 0x50:
      {
         ret = 0x50;
         break;
      }
      case 0x51:
      {
         ret = 0x51;
         break;
      }
      case 0x52:
      {
         ret = 0x52;
         break;
      }
      case 0x53:
      {
         ret = 0x53;
         break;
      }
      case 0x54:
      {
         ret = 0x54;
         break;
      }
      case 0x55:
      {
         ret = 0x55;
         break;
      }
      case 0x56:
      {
         ret = 0x56;
         break;
      }
      case 0x57:
      {
         ret = 0x57;
         break;
      }
      case 0x58:
      {
         ret = 0x58;
         break;
      }
      case 0x59:
      {
         ret = 0x59;
         break;
      }
      case 0x5a:
      {
         ret = 0x5a;
         break;
      }
      case 0x5b:
      {
         ret = 0x5b;
         break;
      }
      case 0x5c:
      {
         ret = 0x5c;
         break;
      }
      case 0x5d:
      {
         ret = 0x5d;
         break;
      }
      case 0x5e:
      {
         ret = 0x5e;
         break;
      }
      case 0x5f:
      {
         ret = 0x5f;
         break;
      }
      case 0x60:
      {
         ret = 0x60;
         break;
      }
      case 0x61:
      {
         ret = 0x61;
         break;
      }
      case 0x62:
      {
         ret = 0x62;
         break;
      }
      case 0x63:
      {
         ret = 0x63;
         break;
      }
      case 0x64:
      {
         ret = 0x64;
         break;
      }
      case 0x65:
      {
         ret = 0x65;
         break;
      }
      case 0x66:
      {
         ret = 0x66;
         break;
      }
      case 0x67:
      {
         ret = 0x67;
         break;
      }
      case 0x68:
      {
         ret = 0x68;
         break;
      }
      case 0x69:
      {
         ret = 0x69;
         break;
      }
      case 0x6a:
      {
         ret = 0x6a;
         break;
      }
      case 0x6b:
      {
         ret = 0x6b;
         break;
      }
      case 0x6c:
      {
         ret = 0x6c;
         break;
      }
      case 0x6d:
      {
         ret = 0x6d;
         break;
      }
      case 0x6e:
      {
         ret = 0x6e;
         break;
      }
      case 0x6f:
      {
         ret = 0x6f;
         break;
      }
      case 0x70:
      {
         ret = 0x70;
         break;
      }
      case 0x71:
      {
         ret = 0x71;
         break;
      }
      case 0x72:
      {
         ret = 0x72;
         break;
      }
      case 0x73:
      {
         ret = 0x73;
         break;
      }
      case 0x74:
      {
         ret = 0x74;
         break;
      }
      case 0x75:
      {
         ret = 0x75;
         break;
      }
      case 0x76:
      {
         ret = 0x76;
         break;
      }
      case 0x77:
      {
         ret = 0x77;
         break;
      }
      case 0x78:
      {
         ret = 0x78;
         break;
      }
      case 0x79:
      {
         ret = 0x79;
         break;
      }
      case 0x7a:
      {
         ret = 0x7a;
         break;
      }
      case 0x7b:
      {
         ret = 0x7b;
         break;
      }
      case 0x7c:
      {
         ret = 0x7c;
         break;
      }
      case 0x7d:
      {
         ret = 0x7d;
         break;
      }
      case 0x7e:
      {
         ret = 0x7e;
         break;
      }
      case 0x7f:
      {
         ret = 0x7f;
         break;
      }
      case 0x80:
      {
         ret = 0x80;
         break;
      }
      case 0x81:
      {
         ret = 0x81;
         break;
      }
      case 0x82:
      {
         ret = 0x82;
         break;
      }
      case 0x83:
      {
         ret = 0x83;
         break;
      }
      case 0x84:
      {
         ret = 0x84;
         break;
      }
      case 0x85:
      {
         ret = 0x85;
         break;
      }
      case 0x86:
      {
         ret = 0x86;
         break;
      }
      case 0x87:
      {
         ret = 0x87;
         break;
      }
      case 0x88:
      {
         ret = 0x88;
         break;
      }
      case 0x89:
      {
         ret = 0x89;
         break;
      }
      case 0x8a:
      {
         ret = 0x8a;
         break;
      }
      case 0x8b:
      {
         ret = 0x8b;
         break;
      }
      case 0x8c:
      {
         ret = 0x8c;
         break;
      }
      case 0x8d:
      {
         ret = 0x8d;
         break;
      }
      case 0x8e:
      {
         ret = 0x8e;
         break;
      }
      case 0x8f:
      {
         ret = 0x8f;
         break;
      }
      case 0x90:
      {
         ret = 0x90;
         break;
      }
      case 0x91:
      {
         ret = 0x91;
         break;
      }
      case 0x92:
      {
         ret = 0x92;
         break;
      }
      case 0x93:
      {
         ret = 0x93;
         break;
      }
      case 0x94:
      {
         ret = 0x94;
         break;
      }
      case 0x95:
      {
         ret = 0x95;
         break;
      }
      case 0x96:
      {
         ret = 0x96;
         break;
      }
      case 0x97:
      {
         ret = 0x97;
         break;
      }
      case 0x98:
      {
         ret = 0x98;
         break;
      }
      case 0x99:
      {
         ret = 0x99;
         break;
      }
      case 0x9a:
      {
         ret = 0x9a;
         break;
      }
      case 0x9b:
      {
         ret = 0x9b;
         break;
      }
      case 0x9c:
      {
         ret = 0x9c;
         break;
      }
      case 0x9d:
      {
         ret = 0x9d;
         break;
      }
      case 0x9e:
      {
         ret = 0x9e;
         break;
      }
      case 0x9f:
      {
         ret = 0x9f;
         break;
      }
      case 0xa0:
      {
         ret = 0xa0;
         break;
      }
      case 0xa1:
      {
         ret = 0xa1;
         break;
      }
      case 0xa2:
      {
         ret = 0xa2;
         break;
      }
      case 0xa3:
      {
         ret = 0xa3;
         break;
      }
      case 0xa4:
      {
         ret = 0xa4;
         break;
      }
      case 0xa5:
      {
         ret = 0xa5;
         break;
      }
      case 0xa6:
      {
         ret = 0xa6;
         break;
      }
      case 0xa7:
      {
         ret = 0xa7;
         break;
      }
      case 0xa8:
      {
         ret = 0xa8;
         break;
      }
      case 0xa9:
      {
         ret = 0xa9;
         break;
      }
      case 0xaa:
      {
         ret = 0xaa;
         break;
      }
      case 0xab:
      {
         ret = 0xab;
         break;
      }
      case 0xac:
      {
         ret = 0xac;
         break;
      }
      case 0xad:
      {
         ret = 0xad;
         break;
      }
      case 0xae:
      {
         ret = 0xae;
         break;
      }
      case 0xaf:
      {
         ret = 0xaf;
         break;
      }
      case 0xb0:
      {
         ret = 0xb0;
         break;
      }
      case 0xb1:
      {
         ret = 0xb1;
         break;
      }
      case 0xb2:
      {
         ret = 0xb2;
         break;
      }
      case 0xb3:
      {
         ret = 0xb3;
         break;
      }
      case 0xb4:
      {
         ret = 0xb4;
         break;
      }
      case 0xb5:
      {
         ret = 0xb5;
         break;
      }
      case 0xb6:
      {
         ret = 0xb6;
         break;
      }
      case 0xb7:
      {
         ret = 0xb7;
         break;
      }
      case 0xb8:
      {
         ret = 0xb8;
         break;
      }
      case 0xb9:
      {
         ret = 0xb9;
         break;
      }
      case 0xba:
      {
         ret = 0xba;
         break;
      }
      case 0xbb:
      {
         ret = 0xbb;
         break;
      }
      case 0xbc:
      {
         ret = 0xbc;
         break;
      }
      case 0xbd:
      {
         ret = 0xbd;
         break;
      }
      case 0xbe:
      {
         ret = 0xbe;
         break;
      }
      case 0xbf:
      {
         ret = 0xbf;
         break;
      }
      case 0xc0:
      {
         ret = 0xc0;
         break;
      }
      case 0xc1:
      {
         ret = 0xc1;
         break;
      }
      case 0xc2:
      {
         ret = 0xc2;
         break;
      }
      case 0xc3:
      {
         ret = 0xc3;
         break;
      }
      case 0xc4:
      {
         ret = 0xc4;
         break;
      }
      case 0xc5:
      {
         ret = 0xc5;
         break;
      }
      case 0xc6:
      {
         ret = 0xc6;
         break;
      }
      case 0xc7:
      {
         ret = 0xc7;
         break;
      }
      case 0xc8:
      {
         ret = 0xc8;
         break;
      }
      case 0xc9:
      {
         ret = 0xc9;
         break;
      }
      case 0xca:
      {
         ret = 0xca;
         break;
      }
      case 0xcb:
      {
         ret = 0xcb;
         break;
      }
      case 0xcc:
      {
         ret = 0xcc;
         break;
      }
      case 0xcd:
      {
         ret = 0xcd;
         break;
      }
      case 0xce:
      {
         ret = 0xce;
         break;
      }
      case 0xcf:
      {
         ret = 0xcf;
         break;
      }
      case 0xd0:
      {
         ret = 0x11e;
         break;
      }
      case 0xd1:
      {
         ret = 0xd1;
         break;
      }
      case 0xd2:
      {
         ret = 0xd2;
         break;
      }
      case 0xd3:
      {
         ret = 0xd3;
         break;
      }
      case 0xd4:
      {
         ret = 0xd4;
         break;
      }
      case 0xd5:
      {
         ret = 0xd5;
         break;
      }
      case 0xd6:
      {
         ret = 0xd6;
         break;
      }
      case 0xd7:
      {
         ret = 0xd7;
         break;
      }
      case 0xd8:
      {
         ret = 0xd8;
         break;
      }
      case 0xd9:
      {
         ret = 0xd9;
         break;
      }
      case 0xda:
      {
         ret = 0xda;
         break;
      }
      case 0xdb:
      {
         ret = 0xdb;
         break;
      }
      case 0xdc:
      {
         ret = 0xdc;
         break;
      }
      case 0xdd:
      {
         ret = 0x130;
         break;
      }
      case 0xde:
      {
         ret = 0x15e;
         break;
      }
      case 0xdf:
      {
         ret = 0xdf;
         break;
      }
      case 0xe0:
      {
         ret = 0xe0;
         break;
      }
      case 0xe1:
      {
         ret = 0xe1;
         break;
      }
      case 0xe2:
      {
         ret = 0xe2;
         break;
      }
      case 0xe3:
      {
         ret = 0xe3;
         break;
      }
      case 0xe4:
      {
         ret = 0xe4;
         break;
      }
      case 0xe5:
      {
         ret = 0xe5;
         break;
      }
      case 0xe6:
      {
         ret = 0xe6;
         break;
      }
      case 0xe7:
      {
         ret = 0xe7;
         break;
      }
      case 0xe8:
      {
         ret = 0xe8;
         break;
      }
      case 0xe9:
      {
         ret = 0xe9;
         break;
      }
      case 0xea:
      {
         ret = 0xea;
         break;
      }
      case 0xeb:
      {
         ret = 0xeb;
         break;
      }
      case 0xec:
      {
         ret = 0xec;
         break;
      }
      case 0xed:
      {
         ret = 0xed;
         break;
      }
      case 0xee:
      {
         ret = 0xee;
         break;
      }
      case 0xef:
      {
         ret = 0xef;
         break;
      }
      case 0xf0:
      {
         ret = 0x11f;
         break;
      }
      case 0xf1:
      {
         ret = 0xf1;
         break;
      }
      case 0xf2:
      {
         ret = 0xf2;
         break;
      }
      case 0xf3:
      {
         ret = 0xf3;
         break;
      }
      case 0xf4:
      {
         ret = 0xf4;
         break;
      }
      case 0xf5:
      {
         ret = 0xf5;
         break;
      }
      case 0xf6:
      {
         ret = 0xf6;
         break;
      }
      case 0xf7:
      {
         ret = 0xf7;
         break;
      }
      case 0xf8:
      {
         ret = 0xf8;
         break;
      }
      case 0xf9:
      {
         ret = 0xf9;
         break;
      }
      case 0xfa:
      {
         ret = 0xfa;
         break;
      }
      case 0xfb:
      {
         ret = 0xfb;
         break;
      }
      case 0xfc:
      {
         ret = 0xfc;
         break;
      }
      case 0xfd:
      {
         ret = 0x131;
         break;
      }
      case 0xfe:
      {
         ret = 0x15f;
         break;
      }
      case 0xff:
      {
         ret = 0xff;
         break;
      }
      default:
      {
         //Do nothing
      }
   }
   return ret;
}


uint32_t ISO8859_9::get_next(const char **str) const
{
   uint32_t ret = 0;
   const char *tmp = *str;
   ret = m_string[m_pos];
   switch(ret)
   {
      case 0x0:
      {
         ret = 0x0;
         break;
      }
      case 0x1:
      {
         ret = 0x1;
         break;
      }
      case 0x2:
      {
         ret = 0x2;
         break;
      }
      case 0x3:
      {
         ret = 0x3;
         break;
      }
      case 0x4:
      {
         ret = 0x4;
         break;
      }
      case 0x5:
      {
         ret = 0x5;
         break;
      }
      case 0x6:
      {
         ret = 0x6;
         break;
      }
      case 0x7:
      {
         ret = 0x7;
         break;
      }
      case 0x8:
      {
         ret = 0x8;
         break;
      }
      case 0x9:
      {
         ret = 0x9;
         break;
      }
      case 0xa:
      {
         ret = 0xa;
         break;
      }
      case 0xb:
      {
         ret = 0xb;
         break;
      }
      case 0xc:
      {
         ret = 0xc;
         break;
      }
      case 0xd:
      {
         ret = 0xd;
         break;
      }
      case 0xe:
      {
         ret = 0xe;
         break;
      }
      case 0xf:
      {
         ret = 0xf;
         break;
      }
      case 0x10:
      {
         ret = 0x10;
         break;
      }
      case 0x11:
      {
         ret = 0x11;
         break;
      }
      case 0x12:
      {
         ret = 0x12;
         break;
      }
      case 0x13:
      {
         ret = 0x13;
         break;
      }
      case 0x14:
      {
         ret = 0x14;
         break;
      }
      case 0x15:
      {
         ret = 0x15;
         break;
      }
      case 0x16:
      {
         ret = 0x16;
         break;
      }
      case 0x17:
      {
         ret = 0x17;
         break;
      }
      case 0x18:
      {
         ret = 0x18;
         break;
      }
      case 0x19:
      {
         ret = 0x19;
         break;
      }
      case 0x1a:
      {
         ret = 0x1a;
         break;
      }
      case 0x1b:
      {
         ret = 0x1b;
         break;
      }
      case 0x1c:
      {
         ret = 0x1c;
         break;
      }
      case 0x1d:
      {
         ret = 0x1d;
         break;
      }
      case 0x1e:
      {
         ret = 0x1e;
         break;
      }
      case 0x1f:
      {
         ret = 0x1f;
         break;
      }
      case 0x20:
      {
         ret = 0x20;
         break;
      }
      case 0x21:
      {
         ret = 0x21;
         break;
      }
      case 0x22:
      {
         ret = 0x22;
         break;
      }
      case 0x23:
      {
         ret = 0x23;
         break;
      }
      case 0x24:
      {
         ret = 0x24;
         break;
      }
      case 0x25:
      {
         ret = 0x25;
         break;
      }
      case 0x26:
      {
         ret = 0x26;
         break;
      }
      case 0x27:
      {
         ret = 0x27;
         break;
      }
      case 0x28:
      {
         ret = 0x28;
         break;
      }
      case 0x29:
      {
         ret = 0x29;
         break;
      }
      case 0x2a:
      {
         ret = 0x2a;
         break;
      }
      case 0x2b:
      {
         ret = 0x2b;
         break;
      }
      case 0x2c:
      {
         ret = 0x2c;
         break;
      }
      case 0x2d:
      {
         ret = 0x2d;
         break;
      }
      case 0x2e:
      {
         ret = 0x2e;
         break;
      }
      case 0x2f:
      {
         ret = 0x2f;
         break;
      }
      case 0x30:
      {
         ret = 0x30;
         break;
      }
      case 0x31:
      {
         ret = 0x31;
         break;
      }
      case 0x32:
      {
         ret = 0x32;
         break;
      }
      case 0x33:
      {
         ret = 0x33;
         break;
      }
      case 0x34:
      {
         ret = 0x34;
         break;
      }
      case 0x35:
      {
         ret = 0x35;
         break;
      }
      case 0x36:
      {
         ret = 0x36;
         break;
      }
      case 0x37:
      {
         ret = 0x37;
         break;
      }
      case 0x38:
      {
         ret = 0x38;
         break;
      }
      case 0x39:
      {
         ret = 0x39;
         break;
      }
      case 0x3a:
      {
         ret = 0x3a;
         break;
      }
      case 0x3b:
      {
         ret = 0x3b;
         break;
      }
      case 0x3c:
      {
         ret = 0x3c;
         break;
      }
      case 0x3d:
      {
         ret = 0x3d;
         break;
      }
      case 0x3e:
      {
         ret = 0x3e;
         break;
      }
      case 0x3f:
      {
         ret = 0x3f;
         break;
      }
      case 0x40:
      {
         ret = 0x40;
         break;
      }
      case 0x41:
      {
         ret = 0x41;
         break;
      }
      case 0x42:
      {
         ret = 0x42;
         break;
      }
      case 0x43:
      {
         ret = 0x43;
         break;
      }
      case 0x44:
      {
         ret = 0x44;
         break;
      }
      case 0x45:
      {
         ret = 0x45;
         break;
      }
      case 0x46:
      {
         ret = 0x46;
         break;
      }
      case 0x47:
      {
         ret = 0x47;
         break;
      }
      case 0x48:
      {
         ret = 0x48;
         break;
      }
      case 0x49:
      {
         ret = 0x49;
         break;
      }
      case 0x4a:
      {
         ret = 0x4a;
         break;
      }
      case 0x4b:
      {
         ret = 0x4b;
         break;
      }
      case 0x4c:
      {
         ret = 0x4c;
         break;
      }
      case 0x4d:
      {
         ret = 0x4d;
         break;
      }
      case 0x4e:
      {
         ret = 0x4e;
         break;
      }
      case 0x4f:
      {
         ret = 0x4f;
         break;
      }
      case 0x50:
      {
         ret = 0x50;
         break;
      }
      case 0x51:
      {
         ret = 0x51;
         break;
      }
      case 0x52:
      {
         ret = 0x52;
         break;
      }
      case 0x53:
      {
         ret = 0x53;
         break;
      }
      case 0x54:
      {
         ret = 0x54;
         break;
      }
      case 0x55:
      {
         ret = 0x55;
         break;
      }
      case 0x56:
      {
         ret = 0x56;
         break;
      }
      case 0x57:
      {
         ret = 0x57;
         break;
      }
      case 0x58:
      {
         ret = 0x58;
         break;
      }
      case 0x59:
      {
         ret = 0x59;
         break;
      }
      case 0x5a:
      {
         ret = 0x5a;
         break;
      }
      case 0x5b:
      {
         ret = 0x5b;
         break;
      }
      case 0x5c:
      {
         ret = 0x5c;
         break;
      }
      case 0x5d:
      {
         ret = 0x5d;
         break;
      }
      case 0x5e:
      {
         ret = 0x5e;
         break;
      }
      case 0x5f:
      {
         ret = 0x5f;
         break;
      }
      case 0x60:
      {
         ret = 0x60;
         break;
      }
      case 0x61:
      {
         ret = 0x61;
         break;
      }
      case 0x62:
      {
         ret = 0x62;
         break;
      }
      case 0x63:
      {
         ret = 0x63;
         break;
      }
      case 0x64:
      {
         ret = 0x64;
         break;
      }
      case 0x65:
      {
         ret = 0x65;
         break;
      }
      case 0x66:
      {
         ret = 0x66;
         break;
      }
      case 0x67:
      {
         ret = 0x67;
         break;
      }
      case 0x68:
      {
         ret = 0x68;
         break;
      }
      case 0x69:
      {
         ret = 0x69;
         break;
      }
      case 0x6a:
      {
         ret = 0x6a;
         break;
      }
      case 0x6b:
      {
         ret = 0x6b;
         break;
      }
      case 0x6c:
      {
         ret = 0x6c;
         break;
      }
      case 0x6d:
      {
         ret = 0x6d;
         break;
      }
      case 0x6e:
      {
         ret = 0x6e;
         break;
      }
      case 0x6f:
      {
         ret = 0x6f;
         break;
      }
      case 0x70:
      {
         ret = 0x70;
         break;
      }
      case 0x71:
      {
         ret = 0x71;
         break;
      }
      case 0x72:
      {
         ret = 0x72;
         break;
      }
      case 0x73:
      {
         ret = 0x73;
         break;
      }
      case 0x74:
      {
         ret = 0x74;
         break;
      }
      case 0x75:
      {
         ret = 0x75;
         break;
      }
      case 0x76:
      {
         ret = 0x76;
         break;
      }
      case 0x77:
      {
         ret = 0x77;
         break;
      }
      case 0x78:
      {
         ret = 0x78;
         break;
      }
      case 0x79:
      {
         ret = 0x79;
         break;
      }
      case 0x7a:
      {
         ret = 0x7a;
         break;
      }
      case 0x7b:
      {
         ret = 0x7b;
         break;
      }
      case 0x7c:
      {
         ret = 0x7c;
         break;
      }
      case 0x7d:
      {
         ret = 0x7d;
         break;
      }
      case 0x7e:
      {
         ret = 0x7e;
         break;
      }
      case 0x7f:
      {
         ret = 0x7f;
         break;
      }
      case 0x80:
      {
         ret = 0x80;
         break;
      }
      case 0x81:
      {
         ret = 0x81;
         break;
      }
      case 0x82:
      {
         ret = 0x82;
         break;
      }
      case 0x83:
      {
         ret = 0x83;
         break;
      }
      case 0x84:
      {
         ret = 0x84;
         break;
      }
      case 0x85:
      {
         ret = 0x85;
         break;
      }
      case 0x86:
      {
         ret = 0x86;
         break;
      }
      case 0x87:
      {
         ret = 0x87;
         break;
      }
      case 0x88:
      {
         ret = 0x88;
         break;
      }
      case 0x89:
      {
         ret = 0x89;
         break;
      }
      case 0x8a:
      {
         ret = 0x8a;
         break;
      }
      case 0x8b:
      {
         ret = 0x8b;
         break;
      }
      case 0x8c:
      {
         ret = 0x8c;
         break;
      }
      case 0x8d:
      {
         ret = 0x8d;
         break;
      }
      case 0x8e:
      {
         ret = 0x8e;
         break;
      }
      case 0x8f:
      {
         ret = 0x8f;
         break;
      }
      case 0x90:
      {
         ret = 0x90;
         break;
      }
      case 0x91:
      {
         ret = 0x91;
         break;
      }
      case 0x92:
      {
         ret = 0x92;
         break;
      }
      case 0x93:
      {
         ret = 0x93;
         break;
      }
      case 0x94:
      {
         ret = 0x94;
         break;
      }
      case 0x95:
      {
         ret = 0x95;
         break;
      }
      case 0x96:
      {
         ret = 0x96;
         break;
      }
      case 0x97:
      {
         ret = 0x97;
         break;
      }
      case 0x98:
      {
         ret = 0x98;
         break;
      }
      case 0x99:
      {
         ret = 0x99;
         break;
      }
      case 0x9a:
      {
         ret = 0x9a;
         break;
      }
      case 0x9b:
      {
         ret = 0x9b;
         break;
      }
      case 0x9c:
      {
         ret = 0x9c;
         break;
      }
      case 0x9d:
      {
         ret = 0x9d;
         break;
      }
      case 0x9e:
      {
         ret = 0x9e;
         break;
      }
      case 0x9f:
      {
         ret = 0x9f;
         break;
      }
      case 0xa0:
      {
         ret = 0xa0;
         break;
      }
      case 0xa1:
      {
         ret = 0xa1;
         break;
      }
      case 0xa2:
      {
         ret = 0xa2;
         break;
      }
      case 0xa3:
      {
         ret = 0xa3;
         break;
      }
      case 0xa4:
      {
         ret = 0xa4;
         break;
      }
      case 0xa5:
      {
         ret = 0xa5;
         break;
      }
      case 0xa6:
      {
         ret = 0xa6;
         break;
      }
      case 0xa7:
      {
         ret = 0xa7;
         break;
      }
      case 0xa8:
      {
         ret = 0xa8;
         break;
      }
      case 0xa9:
      {
         ret = 0xa9;
         break;
      }
      case 0xaa:
      {
         ret = 0xaa;
         break;
      }
      case 0xab:
      {
         ret = 0xab;
         break;
      }
      case 0xac:
      {
         ret = 0xac;
         break;
      }
      case 0xad:
      {
         ret = 0xad;
         break;
      }
      case 0xae:
      {
         ret = 0xae;
         break;
      }
      case 0xaf:
      {
         ret = 0xaf;
         break;
      }
      case 0xb0:
      {
         ret = 0xb0;
         break;
      }
      case 0xb1:
      {
         ret = 0xb1;
         break;
      }
      case 0xb2:
      {
         ret = 0xb2;
         break;
      }
      case 0xb3:
      {
         ret = 0xb3;
         break;
      }
      case 0xb4:
      {
         ret = 0xb4;
         break;
      }
      case 0xb5:
      {
         ret = 0xb5;
         break;
      }
      case 0xb6:
      {
         ret = 0xb6;
         break;
      }
      case 0xb7:
      {
         ret = 0xb7;
         break;
      }
      case 0xb8:
      {
         ret = 0xb8;
         break;
      }
      case 0xb9:
      {
         ret = 0xb9;
         break;
      }
      case 0xba:
      {
         ret = 0xba;
         break;
      }
      case 0xbb:
      {
         ret = 0xbb;
         break;
      }
      case 0xbc:
      {
         ret = 0xbc;
         break;
      }
      case 0xbd:
      {
         ret = 0xbd;
         break;
      }
      case 0xbe:
      {
         ret = 0xbe;
         break;
      }
      case 0xbf:
      {
         ret = 0xbf;
         break;
      }
      case 0xc0:
      {
         ret = 0xc0;
         break;
      }
      case 0xc1:
      {
         ret = 0xc1;
         break;
      }
      case 0xc2:
      {
         ret = 0xc2;
         break;
      }
      case 0xc3:
      {
         ret = 0xc3;
         break;
      }
      case 0xc4:
      {
         ret = 0xc4;
         break;
      }
      case 0xc5:
      {
         ret = 0xc5;
         break;
      }
      case 0xc6:
      {
         ret = 0xc6;
         break;
      }
      case 0xc7:
      {
         ret = 0xc7;
         break;
      }
      case 0xc8:
      {
         ret = 0xc8;
         break;
      }
      case 0xc9:
      {
         ret = 0xc9;
         break;
      }
      case 0xca:
      {
         ret = 0xca;
         break;
      }
      case 0xcb:
      {
         ret = 0xcb;
         break;
      }
      case 0xcc:
      {
         ret = 0xcc;
         break;
      }
      case 0xcd:
      {
         ret = 0xcd;
         break;
      }
      case 0xce:
      {
         ret = 0xce;
         break;
      }
      case 0xcf:
      {
         ret = 0xcf;
         break;
      }
      case 0xd0:
      {
         ret = 0x11e;
         break;
      }
      case 0xd1:
      {
         ret = 0xd1;
         break;
      }
      case 0xd2:
      {
         ret = 0xd2;
         break;
      }
      case 0xd3:
      {
         ret = 0xd3;
         break;
      }
      case 0xd4:
      {
         ret = 0xd4;
         break;
      }
      case 0xd5:
      {
         ret = 0xd5;
         break;
      }
      case 0xd6:
      {
         ret = 0xd6;
         break;
      }
      case 0xd7:
      {
         ret = 0xd7;
         break;
      }
      case 0xd8:
      {
         ret = 0xd8;
         break;
      }
      case 0xd9:
      {
         ret = 0xd9;
         break;
      }
      case 0xda:
      {
         ret = 0xda;
         break;
      }
      case 0xdb:
      {
         ret = 0xdb;
         break;
      }
      case 0xdc:
      {
         ret = 0xdc;
         break;
      }
      case 0xdd:
      {
         ret = 0x130;
         break;
      }
      case 0xde:
      {
         ret = 0x15e;
         break;
      }
      case 0xdf:
      {
         ret = 0xdf;
         break;
      }
      case 0xe0:
      {
         ret = 0xe0;
         break;
      }
      case 0xe1:
      {
         ret = 0xe1;
         break;
      }
      case 0xe2:
      {
         ret = 0xe2;
         break;
      }
      case 0xe3:
      {
         ret = 0xe3;
         break;
      }
      case 0xe4:
      {
         ret = 0xe4;
         break;
      }
      case 0xe5:
      {
         ret = 0xe5;
         break;
      }
      case 0xe6:
      {
         ret = 0xe6;
         break;
      }
      case 0xe7:
      {
         ret = 0xe7;
         break;
      }
      case 0xe8:
      {
         ret = 0xe8;
         break;
      }
      case 0xe9:
      {
         ret = 0xe9;
         break;
      }
      case 0xea:
      {
         ret = 0xea;
         break;
      }
      case 0xeb:
      {
         ret = 0xeb;
         break;
      }
      case 0xec:
      {
         ret = 0xec;
         break;
      }
      case 0xed:
      {
         ret = 0xed;
         break;
      }
      case 0xee:
      {
         ret = 0xee;
         break;
      }
      case 0xef:
      {
         ret = 0xef;
         break;
      }
      case 0xf0:
      {
         ret = 0x11f;
         break;
      }
      case 0xf1:
      {
         ret = 0xf1;
         break;
      }
      case 0xf2:
      {
         ret = 0xf2;
         break;
      }
      case 0xf3:
      {
         ret = 0xf3;
         break;
      }
      case 0xf4:
      {
         ret = 0xf4;
         break;
      }
      case 0xf5:
      {
         ret = 0xf5;
         break;
      }
      case 0xf6:
      {
         ret = 0xf6;
         break;
      }
      case 0xf7:
      {
         ret = 0xf7;
         break;
      }
      case 0xf8:
      {
         ret = 0xf8;
         break;
      }
      case 0xf9:
      {
         ret = 0xf9;
         break;
      }
      case 0xfa:
      {
         ret = 0xfa;
         break;
      }
      case 0xfb:
      {
         ret = 0xfb;
         break;
      }
      case 0xfc:
      {
         ret = 0xfc;
         break;
      }
      case 0xfd:
      {
         ret = 0x131;
         break;
      }
      case 0xfe:
      {
         ret = 0x15f;
         break;
      }
      case 0xff:
      {
         ret = 0xff;
         break;
      }
      default:
      {
         //Do nothing
      }
   }
   tmp++;
   *str = tmp;
   return ret;
}


bool ISO8859_9::add_unicode(uint32_t U)
{
   bool ret = true;
   uint8_t var = 0;
   switch(U)
   {
      case 0x0:
      {
         var = 0x0;
         break;
      }
      case 0x1:
      {
         var = 0x1;
         break;
      }
      case 0x2:
      {
         var = 0x2;
         break;
      }
      case 0x3:
      {
         var = 0x3;
         break;
      }
      case 0x4:
      {
         var = 0x4;
         break;
      }
      case 0x5:
      {
         var = 0x5;
         break;
      }
      case 0x6:
      {
         var = 0x6;
         break;
      }
      case 0x7:
      {
         var = 0x7;
         break;
      }
      case 0x8:
      {
         var = 0x8;
         break;
      }
      case 0x9:
      {
         var = 0x9;
         break;
      }
      case 0xa:
      {
         var = 0xa;
         break;
      }
      case 0xb:
      {
         var = 0xb;
         break;
      }
      case 0xc:
      {
         var = 0xc;
         break;
      }
      case 0xd:
      {
         var = 0xd;
         break;
      }
      case 0xe:
      {
         var = 0xe;
         break;
      }
      case 0xf:
      {
         var = 0xf;
         break;
      }
      case 0x10:
      {
         var = 0x10;
         break;
      }
      case 0x11:
      {
         var = 0x11;
         break;
      }
      case 0x12:
      {
         var = 0x12;
         break;
      }
      case 0x13:
      {
         var = 0x13;
         break;
      }
      case 0x14:
      {
         var = 0x14;
         break;
      }
      case 0x15:
      {
         var = 0x15;
         break;
      }
      case 0x16:
      {
         var = 0x16;
         break;
      }
      case 0x17:
      {
         var = 0x17;
         break;
      }
      case 0x18:
      {
         var = 0x18;
         break;
      }
      case 0x19:
      {
         var = 0x19;
         break;
      }
      case 0x1a:
      {
         var = 0x1a;
         break;
      }
      case 0x1b:
      {
         var = 0x1b;
         break;
      }
      case 0x1c:
      {
         var = 0x1c;
         break;
      }
      case 0x1d:
      {
         var = 0x1d;
         break;
      }
      case 0x1e:
      {
         var = 0x1e;
         break;
      }
      case 0x1f:
      {
         var = 0x1f;
         break;
      }
      case 0x20:
      {
         var = 0x20;
         break;
      }
      case 0x21:
      {
         var = 0x21;
         break;
      }
      case 0x22:
      {
         var = 0x22;
         break;
      }
      case 0x23:
      {
         var = 0x23;
         break;
      }
      case 0x24:
      {
         var = 0x24;
         break;
      }
      case 0x25:
      {
         var = 0x25;
         break;
      }
      case 0x26:
      {
         var = 0x26;
         break;
      }
      case 0x27:
      {
         var = 0x27;
         break;
      }
      case 0x28:
      {
         var = 0x28;
         break;
      }
      case 0x29:
      {
         var = 0x29;
         break;
      }
      case 0x2a:
      {
         var = 0x2a;
         break;
      }
      case 0x2b:
      {
         var = 0x2b;
         break;
      }
      case 0x2c:
      {
         var = 0x2c;
         break;
      }
      case 0x2d:
      {
         var = 0x2d;
         break;
      }
      case 0x2e:
      {
         var = 0x2e;
         break;
      }
      case 0x2f:
      {
         var = 0x2f;
         break;
      }
      case 0x30:
      {
         var = 0x30;
         break;
      }
      case 0x31:
      {
         var = 0x31;
         break;
      }
      case 0x32:
      {
         var = 0x32;
         break;
      }
      case 0x33:
      {
         var = 0x33;
         break;
      }
      case 0x34:
      {
         var = 0x34;
         break;
      }
      case 0x35:
      {
         var = 0x35;
         break;
      }
      case 0x36:
      {
         var = 0x36;
         break;
      }
      case 0x37:
      {
         var = 0x37;
         break;
      }
      case 0x38:
      {
         var = 0x38;
         break;
      }
      case 0x39:
      {
         var = 0x39;
         break;
      }
      case 0x3a:
      {
         var = 0x3a;
         break;
      }
      case 0x3b:
      {
         var = 0x3b;
         break;
      }
      case 0x3c:
      {
         var = 0x3c;
         break;
      }
      case 0x3d:
      {
         var = 0x3d;
         break;
      }
      case 0x3e:
      {
         var = 0x3e;
         break;
      }
      case 0x3f:
      {
         var = 0x3f;
         break;
      }
      case 0x40:
      {
         var = 0x40;
         break;
      }
      case 0x41:
      {
         var = 0x41;
         break;
      }
      case 0x42:
      {
         var = 0x42;
         break;
      }
      case 0x43:
      {
         var = 0x43;
         break;
      }
      case 0x44:
      {
         var = 0x44;
         break;
      }
      case 0x45:
      {
         var = 0x45;
         break;
      }
      case 0x46:
      {
         var = 0x46;
         break;
      }
      case 0x47:
      {
         var = 0x47;
         break;
      }
      case 0x48:
      {
         var = 0x48;
         break;
      }
      case 0x49:
      {
         var = 0x49;
         break;
      }
      case 0x4a:
      {
         var = 0x4a;
         break;
      }
      case 0x4b:
      {
         var = 0x4b;
         break;
      }
      case 0x4c:
      {
         var = 0x4c;
         break;
      }
      case 0x4d:
      {
         var = 0x4d;
         break;
      }
      case 0x4e:
      {
         var = 0x4e;
         break;
      }
      case 0x4f:
      {
         var = 0x4f;
         break;
      }
      case 0x50:
      {
         var = 0x50;
         break;
      }
      case 0x51:
      {
         var = 0x51;
         break;
      }
      case 0x52:
      {
         var = 0x52;
         break;
      }
      case 0x53:
      {
         var = 0x53;
         break;
      }
      case 0x54:
      {
         var = 0x54;
         break;
      }
      case 0x55:
      {
         var = 0x55;
         break;
      }
      case 0x56:
      {
         var = 0x56;
         break;
      }
      case 0x57:
      {
         var = 0x57;
         break;
      }
      case 0x58:
      {
         var = 0x58;
         break;
      }
      case 0x59:
      {
         var = 0x59;
         break;
      }
      case 0x5a:
      {
         var = 0x5a;
         break;
      }
      case 0x5b:
      {
         var = 0x5b;
         break;
      }
      case 0x5c:
      {
         var = 0x5c;
         break;
      }
      case 0x5d:
      {
         var = 0x5d;
         break;
      }
      case 0x5e:
      {
         var = 0x5e;
         break;
      }
      case 0x5f:
      {
         var = 0x5f;
         break;
      }
      case 0x60:
      {
         var = 0x60;
         break;
      }
      case 0x61:
      {
         var = 0x61;
         break;
      }
      case 0x62:
      {
         var = 0x62;
         break;
      }
      case 0x63:
      {
         var = 0x63;
         break;
      }
      case 0x64:
      {
         var = 0x64;
         break;
      }
      case 0x65:
      {
         var = 0x65;
         break;
      }
      case 0x66:
      {
         var = 0x66;
         break;
      }
      case 0x67:
      {
         var = 0x67;
         break;
      }
      case 0x68:
      {
         var = 0x68;
         break;
      }
      case 0x69:
      {
         var = 0x69;
         break;
      }
      case 0x6a:
      {
         var = 0x6a;
         break;
      }
      case 0x6b:
      {
         var = 0x6b;
         break;
      }
      case 0x6c:
      {
         var = 0x6c;
         break;
      }
      case 0x6d:
      {
         var = 0x6d;
         break;
      }
      case 0x6e:
      {
         var = 0x6e;
         break;
      }
      case 0x6f:
      {
         var = 0x6f;
         break;
      }
      case 0x70:
      {
         var = 0x70;
         break;
      }
      case 0x71:
      {
         var = 0x71;
         break;
      }
      case 0x72:
      {
         var = 0x72;
         break;
      }
      case 0x73:
      {
         var = 0x73;
         break;
      }
      case 0x74:
      {
         var = 0x74;
         break;
      }
      case 0x75:
      {
         var = 0x75;
         break;
      }
      case 0x76:
      {
         var = 0x76;
         break;
      }
      case 0x77:
      {
         var = 0x77;
         break;
      }
      case 0x78:
      {
         var = 0x78;
         break;
      }
      case 0x79:
      {
         var = 0x79;
         break;
      }
      case 0x7a:
      {
         var = 0x7a;
         break;
      }
      case 0x7b:
      {
         var = 0x7b;
         break;
      }
      case 0x7c:
      {
         var = 0x7c;
         break;
      }
      case 0x7d:
      {
         var = 0x7d;
         break;
      }
      case 0x7e:
      {
         var = 0x7e;
         break;
      }
      case 0x7f:
      {
         var = 0x7f;
         break;
      }
      case 0x80:
      {
         var = 0x80;
         break;
      }
      case 0x81:
      {
         var = 0x81;
         break;
      }
      case 0x82:
      {
         var = 0x82;
         break;
      }
      case 0x83:
      {
         var = 0x83;
         break;
      }
      case 0x84:
      {
         var = 0x84;
         break;
      }
      case 0x85:
      {
         var = 0x85;
         break;
      }
      case 0x86:
      {
         var = 0x86;
         break;
      }
      case 0x87:
      {
         var = 0x87;
         break;
      }
      case 0x88:
      {
         var = 0x88;
         break;
      }
      case 0x89:
      {
         var = 0x89;
         break;
      }
      case 0x8a:
      {
         var = 0x8a;
         break;
      }
      case 0x8b:
      {
         var = 0x8b;
         break;
      }
      case 0x8c:
      {
         var = 0x8c;
         break;
      }
      case 0x8d:
      {
         var = 0x8d;
         break;
      }
      case 0x8e:
      {
         var = 0x8e;
         break;
      }
      case 0x8f:
      {
         var = 0x8f;
         break;
      }
      case 0x90:
      {
         var = 0x90;
         break;
      }
      case 0x91:
      {
         var = 0x91;
         break;
      }
      case 0x92:
      {
         var = 0x92;
         break;
      }
      case 0x93:
      {
         var = 0x93;
         break;
      }
      case 0x94:
      {
         var = 0x94;
         break;
      }
      case 0x95:
      {
         var = 0x95;
         break;
      }
      case 0x96:
      {
         var = 0x96;
         break;
      }
      case 0x97:
      {
         var = 0x97;
         break;
      }
      case 0x98:
      {
         var = 0x98;
         break;
      }
      case 0x99:
      {
         var = 0x99;
         break;
      }
      case 0x9a:
      {
         var = 0x9a;
         break;
      }
      case 0x9b:
      {
         var = 0x9b;
         break;
      }
      case 0x9c:
      {
         var = 0x9c;
         break;
      }
      case 0x9d:
      {
         var = 0x9d;
         break;
      }
      case 0x9e:
      {
         var = 0x9e;
         break;
      }
      case 0x9f:
      {
         var = 0x9f;
         break;
      }
      case 0xa0:
      {
         var = 0xa0;
         break;
      }
      case 0xa1:
      {
         var = 0xa1;
         break;
      }
      case 0xa2:
      {
         var = 0xa2;
         break;
      }
      case 0xa3:
      {
         var = 0xa3;
         break;
      }
      case 0xa4:
      {
         var = 0xa4;
         break;
      }
      case 0xa5:
      {
         var = 0xa5;
         break;
      }
      case 0xa6:
      {
         var = 0xa6;
         break;
      }
      case 0xa7:
      {
         var = 0xa7;
         break;
      }
      case 0xa8:
      {
         var = 0xa8;
         break;
      }
      case 0xa9:
      {
         var = 0xa9;
         break;
      }
      case 0xaa:
      {
         var = 0xaa;
         break;
      }
      case 0xab:
      {
         var = 0xab;
         break;
      }
      case 0xac:
      {
         var = 0xac;
         break;
      }
      case 0xad:
      {
         var = 0xad;
         break;
      }
      case 0xae:
      {
         var = 0xae;
         break;
      }
      case 0xaf:
      {
         var = 0xaf;
         break;
      }
      case 0xb0:
      {
         var = 0xb0;
         break;
      }
      case 0xb1:
      {
         var = 0xb1;
         break;
      }
      case 0xb2:
      {
         var = 0xb2;
         break;
      }
      case 0xb3:
      {
         var = 0xb3;
         break;
      }
      case 0xb4:
      {
         var = 0xb4;
         break;
      }
      case 0xb5:
      {
         var = 0xb5;
         break;
      }
      case 0xb6:
      {
         var = 0xb6;
         break;
      }
      case 0xb7:
      {
         var = 0xb7;
         break;
      }
      case 0xb8:
      {
         var = 0xb8;
         break;
      }
      case 0xb9:
      {
         var = 0xb9;
         break;
      }
      case 0xba:
      {
         var = 0xba;
         break;
      }
      case 0xbb:
      {
         var = 0xbb;
         break;
      }
      case 0xbc:
      {
         var = 0xbc;
         break;
      }
      case 0xbd:
      {
         var = 0xbd;
         break;
      }
      case 0xbe:
      {
         var = 0xbe;
         break;
      }
      case 0xbf:
      {
         var = 0xbf;
         break;
      }
      case 0xc0:
      {
         var = 0xc0;
         break;
      }
      case 0xc1:
      {
         var = 0xc1;
         break;
      }
      case 0xc2:
      {
         var = 0xc2;
         break;
      }
      case 0xc3:
      {
         var = 0xc3;
         break;
      }
      case 0xc4:
      {
         var = 0xc4;
         break;
      }
      case 0xc5:
      {
         var = 0xc5;
         break;
      }
      case 0xc6:
      {
         var = 0xc6;
         break;
      }
      case 0xc7:
      {
         var = 0xc7;
         break;
      }
      case 0xc8:
      {
         var = 0xc8;
         break;
      }
      case 0xc9:
      {
         var = 0xc9;
         break;
      }
      case 0xca:
      {
         var = 0xca;
         break;
      }
      case 0xcb:
      {
         var = 0xcb;
         break;
      }
      case 0xcc:
      {
         var = 0xcc;
         break;
      }
      case 0xcd:
      {
         var = 0xcd;
         break;
      }
      case 0xce:
      {
         var = 0xce;
         break;
      }
      case 0xcf:
      {
         var = 0xcf;
         break;
      }
      case 0x11e:
      {
         var = 0xd0;
         break;
      }
      case 0xd1:
      {
         var = 0xd1;
         break;
      }
      case 0xd2:
      {
         var = 0xd2;
         break;
      }
      case 0xd3:
      {
         var = 0xd3;
         break;
      }
      case 0xd4:
      {
         var = 0xd4;
         break;
      }
      case 0xd5:
      {
         var = 0xd5;
         break;
      }
      case 0xd6:
      {
         var = 0xd6;
         break;
      }
      case 0xd7:
      {
         var = 0xd7;
         break;
      }
      case 0xd8:
      {
         var = 0xd8;
         break;
      }
      case 0xd9:
      {
         var = 0xd9;
         break;
      }
      case 0xda:
      {
         var = 0xda;
         break;
      }
      case 0xdb:
      {
         var = 0xdb;
         break;
      }
      case 0xdc:
      {
         var = 0xdc;
         break;
      }
      case 0x130:
      {
         var = 0xdd;
         break;
      }
      case 0x15e:
      {
         var = 0xde;
         break;
      }
      case 0xdf:
      {
         var = 0xdf;
         break;
      }
      case 0xe0:
      {
         var = 0xe0;
         break;
      }
      case 0xe1:
      {
         var = 0xe1;
         break;
      }
      case 0xe2:
      {
         var = 0xe2;
         break;
      }
      case 0xe3:
      {
         var = 0xe3;
         break;
      }
      case 0xe4:
      {
         var = 0xe4;
         break;
      }
      case 0xe5:
      {
         var = 0xe5;
         break;
      }
      case 0xe6:
      {
         var = 0xe6;
         break;
      }
      case 0xe7:
      {
         var = 0xe7;
         break;
      }
      case 0xe8:
      {
         var = 0xe8;
         break;
      }
      case 0xe9:
      {
         var = 0xe9;
         break;
      }
      case 0xea:
      {
         var = 0xea;
         break;
      }
      case 0xeb:
      {
         var = 0xeb;
         break;
      }
      case 0xec:
      {
         var = 0xec;
         break;
      }
      case 0xed:
      {
         var = 0xed;
         break;
      }
      case 0xee:
      {
         var = 0xee;
         break;
      }
      case 0xef:
      {
         var = 0xef;
         break;
      }
      case 0x11f:
      {
         var = 0xf0;
         break;
      }
      case 0xf1:
      {
         var = 0xf1;
         break;
      }
      case 0xf2:
      {
         var = 0xf2;
         break;
      }
      case 0xf3:
      {
         var = 0xf3;
         break;
      }
      case 0xf4:
      {
         var = 0xf4;
         break;
      }
      case 0xf5:
      {
         var = 0xf5;
         break;
      }
      case 0xf6:
      {
         var = 0xf6;
         break;
      }
      case 0xf7:
      {
         var = 0xf7;
         break;
      }
      case 0xf8:
      {
         var = 0xf8;
         break;
      }
      case 0xf9:
      {
         var = 0xf9;
         break;
      }
      case 0xfa:
      {
         var = 0xfa;
         break;
      }
      case 0xfb:
      {
         var = 0xfb;
         break;
      }
      case 0xfc:
      {
         var = 0xfc;
         break;
      }
      case 0x131:
      {
         var = 0xfd;
         break;
      }
      case 0x15f:
      {
         var = 0xfe;
         break;
      }
      case 0xff:
      {
         var = 0xff;
         break;
      }
      default:
      {
         ret = false;
      }
   }
   if(ret)
   {
      pushElem(var);
   }
   return ret;
}
}
