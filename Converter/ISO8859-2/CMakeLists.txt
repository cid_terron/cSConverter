set(ID 2)
set(NAME "ISO8859-${ID}_dynamic")
project(${NAME})

add_definitions(-rdynamic)
add_definitions(-DSCONV_E_ISO8859_${ID}=2+${ID})

set(SOURCES
"ISO8859-${ID}.cpp"
)

set(INCLUDES
"${INCLUDE_OUTPUT_PATH}/${GNAME}/ISO8859-${ID}.h"
)

add_custom_command(
OUTPUT 
"${INCLUDE_OUTPUT_PATH}/${GNAME}/ISO8859-${ID}.h"
PRE_BUILD
COMMAND ${CMAKE_COMMAND} -E make_directory ${INCLUDE_OUTPUT_PATH}/${GNAME}/
COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_CURRENT_SOURCE_DIR}/ISO8859-${ID}.h ${INCLUDE_OUTPUT_PATH}/${GNAME}/
DEPENDS
"ISO8859-${ID}.h"
)


add_library ("${NAME}" SHARED ${SOURCES} ${INCLUDES})

add_dependencies(${NAME} cSConverter)
install(TARGETS "${NAME}" 
RUNTIME DESTINATION "${BINDIR}"
LIBRARY DESTINATION "${LIBDIR}"
)
