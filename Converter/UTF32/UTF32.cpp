/*
 * UTF32.cpp
 *
 *  Created on: Feb 21, 2016
 *      Author: Dominik Frizel
 */

/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
 */

#include "UTF32.h"
#include "string.h"

#include "cSConverter/Common.h"

#include "UTF8/UTF8.h"

namespace cSConverter
{

//U+0000 0000..U+0000 D7FF, U+0000 E000..U+0010 FFFF
UTF32::UTF32(SCONV_ENDIANESS sEndian) : UTF_Common(sEndian)
{
   create_pairs(3);
   if(sEndian == SCONV_E_BIG)
   {
      UTF_PAIRS tmp[4] = {0, 0 , {0, 0xd7}, {0x0, 0xff}};
      //Those are  U+0000 E000..U+0010 FFFF
      //Split into U+0000 E000 ... U+0000 FFFF
      //and        U+0001 0000 ... U+0010 FFFF
      UTF_PAIRS tmp2[4] = {0, 0, {0xe0, 0xff}, {0x0, 0xff}};
      //    UTF_PAIRS tmp3[4] = {0, {0x01, 0x10}, {0x00, 0xff}, {0x0, 0xff}};
      UTF_PAIRS tmp3[4] = {{0, 0}, {0x01, 0x10}, {0x00, 0xff}, {0x0, 0xff}};

      add_pair(tmp, 4);
      add_pair(tmp2, 4);
      add_pair(tmp3, 4);
   }
   else if(sEndian == SCONV_E_LITTLE)
   {
      UTF_PAIRS tmp[4] = {{0x0, 0xff}, {0, 0xd7}, 0, 0};
      UTF_PAIRS tmp2[4] = {{0x0, 0xff}, {0xe0, 0xff}, 0, 0};
      UTF_PAIRS tmp3[4] = {{0x0, 0xff}, {0x00, 0xff}, {0x01, 0x10}, 0};
      add_pair(tmp, 4);
      add_pair(tmp2, 4);
      add_pair(tmp3, 4);
   }
}

UTF32::UTF32(const UTF_Common &other) : UTF_Common(other.get_endianess())
{
   create_pairs(3);
   SCONV_ENDIANESS sEndian = other.get_endianess();
   if(sEndian == SCONV_E_BIG)
   {
      UTF_PAIRS tmp[4] = {0, 0 , {0, 0xd7}, {0x0, 0xff}};
      //Those are  U+0000 E000..U+0010 FFFF
      //Split into U+0000 E000 ... U+0000 FFFF
      //and        U+0001 0000 ... U+0010 FFFF
      UTF_PAIRS tmp2[4] = {0, 0, {0xe0, 0xff}, {0x0, 0xff}};
      //    UTF_PAIRS tmp3[4] = {0, {0x01, 0x10}, {0x00, 0xff}, {0x0, 0xff}};
      UTF_PAIRS tmp3[4] = {{0, 0}, {0x01, 0x10}, {0x00, 0xff}, {0x0, 0xff}};

      add_pair(tmp, 4);
      add_pair(tmp2, 4);
      add_pair(tmp3, 4);
   }
   else if(sEndian == SCONV_E_LITTLE)
   {
      UTF_PAIRS tmp[4] = {{0x0, 0xff}, {0, 0xd7}, 0, 0};
      UTF_PAIRS tmp2[4] = {{0x0, 0xff}, {0xe0, 0xff}, 0, 0};
      UTF_PAIRS tmp3[4] = {{0x0, 0xff}, {0x00, 0xff}, {0x01, 0x10}, 0};
      add_pair(tmp, 4);
      add_pair(tmp2, 4);
      add_pair(tmp3, 4);
   }

   assign(other);
}

UTF32::UTF32(const UTF32 &other) : UTF_Common(other.get_endianess())
{
   create_pairs(3);
   SCONV_ENDIANESS sEndian = other.get_endianess();
   if(sEndian == SCONV_E_BIG)
   {
      UTF_PAIRS tmp[4] = {0, 0 , {0, 0xd7}, {0x0, 0xff}};
      //Those are  U+0000 E000..U+0010 FFFF
      //Split into U+0000 E000 ... U+0000 FFFF
      //and        U+0001 0000 ... U+0010 FFFF
      UTF_PAIRS tmp2[4] = {0, 0, {0xe0, 0xff}, {0x0, 0xff}};
      //    UTF_PAIRS tmp3[4] = {0, {0x01, 0x10}, {0x00, 0xff}, {0x0, 0xff}};
      UTF_PAIRS tmp3[4] = {{0, 0}, {0x01, 0x10}, {0x00, 0xff}, {0x0, 0xff}};

      add_pair(tmp, 4);
      add_pair(tmp2, 4);
      add_pair(tmp3, 4);
   }
   else if(sEndian == SCONV_E_LITTLE)
   {
      UTF_PAIRS tmp[4] = {{0x0, 0xff}, {0, 0xd7}, 0, 0};
      UTF_PAIRS tmp2[4] = {{0x0, 0xff}, {0xe0, 0xff}, 0, 0};
      UTF_PAIRS tmp3[4] = {{0x0, 0xff}, {0x00, 0xff}, {0x01, 0x10}, 0};
      add_pair(tmp, 4);
      add_pair(tmp2, 4);
      add_pair(tmp3, 4);
   }

   assign(other);
}

bool UTF32::validate(const char *str, const size_t& size)
{
   bool ret = false;
   bool quit = false;

   if((size%4 == 0))
   {
      for(size_t i = 0; (i < size); i++)
      {
         ret = iterate(i, quit, str);
         if(quit || (ret != true))
         {
            break;
         }
      }
   }

   return ret;
}

bool UTF32::add_unicode(uint32_t U)
{
   uint8_t FIRST = 0;
   uint8_t SECOND = 0;
   uint8_t THIRD = 0;
   uint8_t FOURTH = 0;

   uint8_t data[4];
   uint32_t tmp = U;
   if(m_endianess == SCONV_E_BIG)
   {
      FIRST  = 0;
      SECOND = 1;
      THIRD  = 2;
      FOURTH = 3;
   }
   else if(m_endianess == SCONV_E_LITTLE)
   {
      FIRST  = 3;
      SECOND = 2;
      THIRD  = 1;
      FOURTH = 0;
   }

   data[FOURTH] = (tmp & 0xff);
   tmp >>= 8;
   data[THIRD] = (tmp & 0xff);
   tmp >>= 8;
   data[SECOND] = (tmp & 0xff);
   tmp >>= 8;
   data[FIRST] = (tmp & 0xff);

   pushElem(data[0]);
   pushElem(data[1]);
   pushElem(data[2]);
   pushElem(data[3]);

   //  m_size+=4;

   return true;
}

uint32_t UTF32::get_next(const char **str) const
{
   uint32_t ret = 0;
   const char *tmp = *str;

   uint8_t FIRST = 0;
   uint8_t SECOND = 0;
   uint8_t THIRD = 0;
   uint8_t FOURTH = 0;

   if(tmp == 0)
   {
      return 0;
   }

   if(m_endianess == SCONV_E_BIG)
   {
      FIRST  = 0;
      SECOND = 1;
      THIRD  = 2;
      FOURTH = 3;
   }
   else
   {
      FIRST  = 3;
      SECOND = 2;
      THIRD  = 1;
      FOURTH = 0;
   }

   //  Codepoint range                   Unicode scalar value    Code units (binary)
   //  U+0000..U+D7FF, U+E000..U+10FFFF  xxxxxxxxxxxxxxxxxxxxx   00000000.000xxxxx.xxxxxxxx.xxxxxxxx

   //000x.xxxx
   if(tmp[FIRST] == 0 && (tmp[SECOND] & 0xE0) == 0)
   {
      ret = tmp[SECOND];
      ret <<= 8;
      ret |= ((uint32_t)tmp[THIRD] & 0x000000FF);
      ret <<= 8;
      ret |= ((uint32_t)tmp[FOURTH] & 0x000000FF);
   }
   else
   {
      ret = 0xFFFFFFFF;
   }

   tmp += 4;
   *str = tmp;

   return ret;
}

uint32_t UTF32::get_next()
{
   uint32_t ret = 0;
   uint8_t *tmp = (uint8_t *)&m_string[m_pos];

   uint8_t FIRST = 0;
   uint8_t SECOND = 0;
   uint8_t THIRD = 0;
   uint8_t FOURTH = 0;

   if(m_endianess == SCONV_E_BIG)
   {
      FIRST  = 0;
      SECOND = 1;
      THIRD  = 2;
      FOURTH = 3;
   }
   else
   {
      FIRST  = 3;
      SECOND = 2;
      THIRD  = 1;
      FOURTH = 0;
   }

   //  Codepoint range                   Unicode scalar value    Code units (binary)
   //  U+0000..U+D7FF, U+E000..U+10FFFF  xxxxxxxxxxxxxxxxxxxxx   00000000.000xxxxx.xxxxxxxx.xxxxxxxx

   //000x.xxxx
   if(tmp[FIRST] == 0 && (tmp[SECOND] & 0xE0) == 0)
   {
      ret = tmp[SECOND];
      ret <<= 8;
      ret |= ((uint32_t)tmp[THIRD] & 0x000000FF);
      ret <<= 8;
      ret |= ((uint32_t)tmp[FOURTH] & 0x000000FF);
   }
   else
   {
      ret = 0xFFFFFFFF;
   }

   m_pos += 4; //just always move by 4

   return ret;
}

#ifdef USE_DLOPEN
bool UTF32::add_cstring(const char *str)
{
   UTF_Common *common = Common::generate(SCONV_E_UTF_8, SCONV_E_SINGLE);
   //There is NO static cast because of DLOPEN !
   UTF8 *tmp = static_cast<UTF8*>(common);
   if(tmp)
   {
      tmp->add_cstring(str);
   }
   append(*tmp);
   cSConverterFree(tmp);
   return true;
}
#else
bool UTF32::add_cstring(const char *str)
{
   UTF8 tmp;
   tmp.add_cstring(str);
   append(tmp);

   return true;
}
#endif

UTF_Common &UTF32::operator+=(UTF32 &other)
      {
   if(m_endianess == other.get_endianess())
   {
      char *creator = 0;
      const char *tmp = other.get_string();
      uint32_t size = other.get_string_size();

      creator = CSCONVERTER_NEW(char[size+get_plain_string_size()]);

      memset(creator, 0, size+get_plain_string_size());
      memcpy(creator, m_string, get_plain_string_size());
      if(tmp)
      {
         memcpy(&creator[get_plain_string_size()], tmp, size);
      }

      //Needs to be here - otherwise A += A will no work!
      if(m_string)
      {
         CSCONVERTER_DELETE2(m_string, char);
      }

      m_size = get_plain_string_size() + size;
      m_string = creator;

      return *this;
   }
   else
   {
      return append(other);
   }
      }

UTF_Common &UTF32::operator+=(UTF_Common &other)
      {
   return append(other);
      }


UTF_Common &UTF32::operator=(const UTF32 &other)
{
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
      m_string = 0;
   }

   m_string = CSCONVERTER_NEW(char[other.get_string_size()]);

   if(m_endianess == other.get_endianess())
   {
      memcpy(m_string, other.get_string(), other.get_string_size());
   }
   else
   {
      const char *tmp = other.get_string();
      for(uint32_t i = 0; i < other.get_string_size(); i+=4)
      {
         m_string[i+3] = tmp[i+0];
         m_string[i+2] = tmp[i+1];
         m_string[i+1] = tmp[i+2];
         m_string[i+0] = tmp[i+3];
      }
   }

   m_size = other.get_string_size();

   return *this;
}

UTF_Common &UTF32::operator=(UTF_Common &other)
{
   return assign(other);
}

UTF_Common &UTF32::operator+=(const char *str)
      {
   add_cstring(str);
   return *this;
      }

UTF_Common &UTF32::operator=(const char *str)
{
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
      m_string = 0;
      m_size = 0;
   }
   add_cstring(str);

   return *this;
}

bool UTF32::set_string(const char *str, const size_t& size)
{
   size_t alloc_size = size;
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
   }

   if(str[size-1] != 0 || str[size-2] != 0 || str[size-3] != 0 || str[size-4] != 0)
   {
      alloc_size += 4;
   }

   m_string = CSCONVERTER_NEW(char[alloc_size]);
   memcpy(m_string, str, size);
   if(alloc_size != size)
   {
      memset(&m_string[size], 0, alloc_size - size);
   }
   m_size = alloc_size;

   return true;
}

const uint32_t *UTF32::get_w32string()
{
   return reinterpret_cast<uint32_t *>(m_string);
}



#ifdef USE_DLOPEN

extern "C" UTF_Common *generate(SCONV_ENDIANESS endianess)
{
   return new UTF32(endianess);
}

extern "C" void *create_library(Common *ptr)
{
   ptr->insert(SCONV_E_UTF_32, "UTF-32", generate);
   return nullptr;
}

extern "C" void assign(Converter_in_if *dst, const char *src)
{
   UTF32 *iso = dynamic_cast<UTF32 *>(dst);
   *iso = src;
}

extern "C" void assign_Common(Converter_in_if *dst, Converter_in_if *src)
{
   UTF32 *iso = dynamic_cast<UTF32 *>(dst);
   UTF_Common *cmn = dynamic_cast<UTF_Common *>(src);
   *iso = *cmn;
}

extern "C" void append(Converter_in_if *dst, const char *src)
{
   UTF32 *iso = dynamic_cast<UTF32 *>(dst);
   *iso += src;
}

extern "C" void append_Common(Converter_in_if *dst, UTF_Common *src)
{
   UTF32 *iso = dynamic_cast<UTF32 *>(dst);
   UTF_Common *cmn = dynamic_cast<UTF_Common *>(src);
   *iso += *cmn;
}

#endif

}
