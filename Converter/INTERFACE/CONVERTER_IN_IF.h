/*
 * CONVERTER_IN_IF.h
 *
 *  Created on: Feb 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef CONVERTER_INTERFACE_CONVERTER_IN_IF_H_
#define CONVERTER_INTERFACE_CONVERTER_IN_IF_H_

#include "stddef.h"

class Converter_in_if
{
  public:
    virtual ~Converter_in_if(){};
    virtual bool validate(const char *txt, const size_t& length) = 0;
    virtual bool add_cstring(const char *str) = 0;
    /**
     *
     */
    virtual bool set_string(const char *str, const size_t& size) = 0;
    virtual const char *get_string() const = 0;
    virtual const size_t get_string_size() const = 0;
};



#endif /* CONVERTER_INTERFACE_CONVERTER_IN_IF_H_ */
