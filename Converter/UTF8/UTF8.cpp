/*
 * UTF8.cpp
 *
 *  Created on: Feb 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
 */

#include "UTF8.h"

//RFC 3629

#include "array"
#include "string.h"
#include "cSConverter/Alloc_Fkt.h"

#include "cSConverter/Common.h"

namespace cSConverter
{

#define UTF_TAIL {0x80, 0xBF}

UTF8::UTF8() : UTF_Common(SCONV_E_SINGLE)
{
   UTF_PAIRS tmp[4] = {{0, 0x7F}, 0, 0, 0};
   UTF_PAIRS tmp2[4] = {{0xC2, 0xDF}, UTF_TAIL, 0, 0};
   UTF_PAIRS tmp3[4] = {{0xE0, 0xE0}, {0xA0, 0xBF}, UTF_TAIL, 0};
   UTF_PAIRS tmp4[4] = {{0xE1, 0xEC}, UTF_TAIL, UTF_TAIL, 0};
   UTF_PAIRS tmp5[4] = {{0xED, 0xED}, {0x80, 0x9F}, UTF_TAIL, 0};
   UTF_PAIRS tmp6[4] = {{0xEE, 0xEF}, UTF_TAIL, UTF_TAIL, 0};
   UTF_PAIRS tmp7[4] = {{0xF0, 0xF0}, {0x90, 0xBF}, UTF_TAIL, UTF_TAIL};
   UTF_PAIRS tmp8[4] = {{0xF1, 0xF3}, UTF_TAIL, UTF_TAIL, UTF_TAIL};
   UTF_PAIRS tmp9[4] = {{0xF4, 0xF4}, {0x80, 0x8F}, UTF_TAIL, UTF_TAIL};

   create_pairs(9);

   add_pair(tmp , 1);
   add_pair(tmp2 , 2);
   add_pair(tmp3 , 3);
   add_pair(tmp4 , 3);
   add_pair(tmp5 , 3);
   add_pair(tmp6 , 3);
   add_pair(tmp7 , 4);
   add_pair(tmp8 , 4);
   add_pair(tmp9 , 4);
}

UTF8::UTF8(const UTF8 &other) : UTF_Common(SCONV_E_SINGLE)
{
   UTF_PAIRS tmp[4] = {{0, 0x7F}, 0, 0, 0};
   UTF_PAIRS tmp2[4] = {{0xC2, 0xDF}, UTF_TAIL, 0, 0};
   UTF_PAIRS tmp3[4] = {{0xE0, 0xE0}, {0xA0, 0xBF}, UTF_TAIL, 0};
   UTF_PAIRS tmp4[4] = {{0xE1, 0xEC}, UTF_TAIL, UTF_TAIL, 0};
   UTF_PAIRS tmp5[4] = {{0xED, 0xED}, {0x80, 0x9F}, UTF_TAIL, 0};
   UTF_PAIRS tmp6[4] = {{0xEE, 0xEF}, UTF_TAIL, UTF_TAIL, 0};
   UTF_PAIRS tmp7[4] = {{0xF0, 0xF0}, {0x90, 0xBF}, UTF_TAIL, UTF_TAIL};
   UTF_PAIRS tmp8[4] = {{0xF1, 0xF3}, UTF_TAIL, UTF_TAIL, UTF_TAIL};
   UTF_PAIRS tmp9[4] = {{0xF4, 0xF4}, {0x80, 0x8F}, UTF_TAIL, UTF_TAIL};

   create_pairs(9);

   add_pair(tmp , 1);
   add_pair(tmp2 , 2);
   add_pair(tmp3 , 3);
   add_pair(tmp4 , 3);
   add_pair(tmp5 , 3);
   add_pair(tmp6 , 3);
   add_pair(tmp7 , 4);
   add_pair(tmp8 , 4);
   add_pair(tmp9 , 4);

   assign(other);

}

UTF8::UTF8(const UTF_Common &other) : UTF_Common(SCONV_E_SINGLE)
{
   UTF_PAIRS tmp[4] = {{0, 0x7F}, 0, 0, 0};
   UTF_PAIRS tmp2[4] = {{0xC2, 0xDF}, UTF_TAIL, 0, 0};
   UTF_PAIRS tmp3[4] = {{0xE0, 0xE0}, {0xA0, 0xBF}, UTF_TAIL, 0};
   UTF_PAIRS tmp4[4] = {{0xE1, 0xEC}, UTF_TAIL, UTF_TAIL, 0};
   UTF_PAIRS tmp5[4] = {{0xED, 0xED}, {0x80, 0x9F}, UTF_TAIL, 0};
   UTF_PAIRS tmp6[4] = {{0xEE, 0xEF}, UTF_TAIL, UTF_TAIL, 0};
   UTF_PAIRS tmp7[4] = {{0xF0, 0xF0}, {0x90, 0xBF}, UTF_TAIL, UTF_TAIL};
   UTF_PAIRS tmp8[4] = {{0xF1, 0xF3}, UTF_TAIL, UTF_TAIL, UTF_TAIL};
   UTF_PAIRS tmp9[4] = {{0xF4, 0xF4}, {0x80, 0x8F}, UTF_TAIL, UTF_TAIL};

   create_pairs(9);

   add_pair(tmp , 1);
   add_pair(tmp2 , 2);
   add_pair(tmp3 , 3);
   add_pair(tmp4 , 3);
   add_pair(tmp5 , 3);
   add_pair(tmp6 , 3);
   add_pair(tmp7 , 4);
   add_pair(tmp8 , 4);
   add_pair(tmp9 , 4);

   assign(other);
}

UTF8::UTF8(const char *str) : UTF_Common(SCONV_E_SINGLE)
{
   UTF_PAIRS tmp[4] = {{0, 0x7F}, 0, 0, 0};
   UTF_PAIRS tmp2[4] = {{0xC2, 0xDF}, UTF_TAIL, 0, 0};
   UTF_PAIRS tmp3[4] = {{0xE0, 0xE0}, {0xA0, 0xBF}, UTF_TAIL, 0};
   UTF_PAIRS tmp4[4] = {{0xE1, 0xEC}, UTF_TAIL, UTF_TAIL, 0};
   UTF_PAIRS tmp5[4] = {{0xED, 0xED}, {0x80, 0x9F}, UTF_TAIL, 0};
   UTF_PAIRS tmp6[4] = {{0xEE, 0xEF}, UTF_TAIL, UTF_TAIL, 0};
   UTF_PAIRS tmp7[4] = {{0xF0, 0xF0}, {0x90, 0xBF}, UTF_TAIL, UTF_TAIL};
   UTF_PAIRS tmp8[4] = {{0xF1, 0xF3}, UTF_TAIL, UTF_TAIL, UTF_TAIL};
   UTF_PAIRS tmp9[4] = {{0xF4, 0xF4}, {0x80, 0x8F}, UTF_TAIL, UTF_TAIL};

   create_pairs(9);

   add_pair(tmp , 1);
   add_pair(tmp2 , 2);
   add_pair(tmp3 , 3);
   add_pair(tmp4 , 3);
   add_pair(tmp5 , 3);
   add_pair(tmp6 , 3);
   add_pair(tmp7 , 4);
   add_pair(tmp8 , 4);
   add_pair(tmp9 , 4);

   add_cstring(str);
}

bool UTF8::validate(const char *str, const size_t& size)
{
   bool ret = false;
   bool quit = false;

   if(size > 0)
   {
      for(size_t i = 0; (i < size); i++)
      {
         ret = iterate(i, quit, str);

         if(quit || (ret != true))
         {
            break;
         }
      }
   }

   return ret;
}

//UNICODE                         BINARY                  BYTE 1    BYTE 2    BYTE 3    BYTE 4
//U+0000..U+007F                  00000000000000xxxxxxx   0xxxxxxx
//U+0080..U+07FF                  0000000000yyyyyxxxxxx   110yyyyy  10xxxxxx
//U+0800..U+D7FF, U+E000..U+FFFF  00000zzzzyyyyyyxxxxxx   1110zzzz  10yyyyyy  10xxxxxx
//U+10000..U+10FFFF               uuuzzzzzzyyyyyyxxxxxx   11110uuu  10zzzzzz  10yyyyyy  10xxxxxx

bool UTF8::add_unicode(uint32_t U)
{
   bool ret = true;
   //00000000.00000000.00000000.0xxxxxxx
   if((U & 0xFFFFFF80) == 0)
   {
      //0xxx.xxxx
      if(U != 0)
      {
         uint8_t tmp = (U & 0x7F);
         pushElem(tmp);
      }
   }
   else if((U & 0xFFFFF800) == 0) //00000000.00000000.0000-0yyy.yyxx-xxxx
   {
      //110y.yyyy  10xx.xxxx
      uint16_t tmp = (U & 0x07c0);
      //110y.yyyy
      uint8_t tmp2 = 0xc0; //add 1100.0000
      tmp >>= 6; //move y to right by x
      tmp2 |= (tmp & 0x1F); //this is 000y.yyyy
      pushElem(tmp2);// add first byte;
      tmp = (U & 0x3F); //tmp is now 00xx.xxxx
      tmp2 = 0x80; //tmp2 is now 1000.0000
      tmp2 |= tmp;
      pushElem(tmp2);
   }
   else if((U>=0x0800 && U<=0xD7FF) || (U>=0xE000 && U<=0xFFFF)) //00000000.00000000.zzzzyyyy.yyxxxxxx
   {
      //zzzz.yyyy
      uint16_t tmp = (U & 0xF000); //tmp is now zzzz0000.00000000
      tmp >>= 12; //now we have 00000000.0000zzzz
      //1110.zzzz
      uint8_t tmp2 = 0xe0; //1110.0000
      tmp2 |= (tmp & 0x0f);
      pushElem(tmp2);
      tmp = (U & 0x0FC0); // 0000-yyyy.yy00-0000
      tmp >>= 6; //0000-0000-00yy.yyyy
      tmp2 = 0x80; //1000.0000
      tmp2 |= (tmp & 0x3f); //10yy.yyyy
      pushElem(tmp2);
      //yyxx.xxxx
      tmp2 = (U & 0x3f);//00xx.xxxx
      tmp2 |= 0x80;
      pushElem(tmp2);
   }
   else if(U >= 0x10000 && U <= 0x10FFFF)
   {
      //00000000.000uuuzz.zzzzyyyy.yyxxxxxx
      uint32_t tmp = (U & 0x1C0000); //000u-uu00.00000000.00000000
      tmp >>= 18; // 00000000.00000000.00000000.00000uuu
      uint8_t tmp2 = (tmp & 0x7); //1111.0uuu
      tmp2 |= 0xF0;
      pushElem(tmp2);
      //000u-uuzz.zzzz-yyyy.yyxxxxxx
      tmp = (U & 0x03F000); //000000zz.zzzz0000.00000000
      tmp >>= 12; //00zz.zzzz
      tmp2 = (tmp & 0x3F);
      //10zz.zzzz
      tmp2 |= 0x80;
      //zzzz-yyyy.yyxx-xxxx
      tmp = (U & 0x0FC0);
      tmp >>= 6;
      tmp2 = (tmp & 0x3F);
      tmp2 |= 0x80; //10yyyyyy
      pushElem(tmp2);
      //yyxx-xxxx
      tmp2 = (U & 0x3F);
      tmp2 |= 0x80;
      pushElem(tmp2);
   }
   else
   {
      ret = false;
   }

   return ret;
}

bool UTF8::add_cstring(const char *str)
{
   uint32_t len = 0;
   const char *tmp = str;
   char *creator = 0;

   if(str[0] == '0')
   {
      CSCONVERTER_DELETE2(m_string, char);
      m_string = 0;
      m_size = 0;
      return true;
   }

   while(tmp[0] != 0)
   {
      len++;
      tmp++;
   }

   creator = CSCONVERTER_NEW(char[m_size + len + 1]);
   memcpy(creator, m_string, m_size);
   memcpy(&creator[m_size], str, len);
   m_size = len + m_size;
   creator[m_size] = 0;
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
   }
   m_string = creator;

   return true;
}


//128 64 32 16 . 8 4 2 1

uint32_t UTF8::get_next(const char **str) const
{
   const char *tmp = *str;
   uint32_t ret = 0;
   uint32_t offset = 0;

   if(tmp == 0)
   {
      return 0;
   }

   if((tmp[0] & 0x80) == 0) //0xxxxxxx 0 on first bit
   {
      ret = tmp[0];
      offset++;
   }
   else if((tmp[0] & 0x40) == 0)//10xxxxxx not valid - 0 on second bit
   {
      ret = 0xFFFFFFFF; //32bit INVALID
   }
   else if((tmp[0] & 0x20) == 0)//110y.yyyy - 0 on third bit
   {
      if((tmp[1] & 0xc0) == 0x80) //10xx.xxxx
      {
         ret = ((uint32_t)tmp[0] & 0x1F);
         ret <<= 6; //Shift by possible x-bits
         ret |= ((uint32_t)tmp[1] & 0x3F);
         offset+=2;
      }
      else
      {
         ret = 0xFFFFFFFF;
      }
   }
   else if((tmp[0] & 0x10) == 0x00) //1110zzzz - 0 on fourth bit
   {
      if(((tmp[1] & 0xc0) == 0x80) && ((tmp[2] & 0xC0) == 0x80))
      {
         //        10yyyyyy  10xxxxxx
         ret = ((uint32_t)tmp[0] & 0x0F);
         ret <<= 6; //shift by y
         ret |= ((uint32_t)tmp[1] & 0x3F);
         ret <<= 6; //shift by x
         ret |= ((uint32_t)tmp[2] & 0x3F);
         offset+=3;
      }
      else
      {
         ret = 0xFFFFFFFF;
      }
   }
   else if((tmp[0] & 0x08) == 0x00) //11110uuu - 0 on fifth bit
   {
      if(((tmp[1] & 0xc0) == 0x80) && ((tmp[2] & 0xc0) == 0x80) && ((tmp[3] & 0xc0) == 0x80))
      {
         ret = ((uint32_t)tmp[0] & 0x0F);
         ret <<= 6; //shift by z
         ret |= ((uint32_t)tmp[1] & 0x3F);
         ret <<= 6; //shift by y
         ret |= ((uint32_t)tmp[2] & 0x3F);
         ret <<= 6; //shift by x
         ret |= ((uint32_t)tmp[3] & 0x3F);
         offset+=4;
      }
   }
   tmp += offset;
   *str = tmp;

   return ret;
}

uint32_t UTF8::get_next()
{
   uint8_t *tmp = (uint8_t *)&m_string[m_pos];
   uint32_t ret = 0;
   uint32_t offset = 0;

   if((tmp[0] & 0x80) == 0) //0xxxxxxx 0 on first bit
   {
      ret = tmp[0];
      offset++;
   }
   else if((tmp[0] & 0x40) == 0)//10xxxxxx not valid - 0 on second bit
   {
      ret = 0xFFFFFFFF; //32bit INVALID
   }
   else if((tmp[0] & 0x20) == 0)//110y.yyyy - 0 on third bit
   {
      if((tmp[1] & 0xc0) == 0x80) //10xx.xxxx
      {
         ret = ((uint32_t)tmp[0] & 0x1F);
         ret <<= 6; //Shift by possible x-bits
         ret |= ((uint32_t)tmp[1] & 0x3F);
         offset+=2;
      }
      else
      {
         ret = 0xFFFFFFFF;
      }
   }
   else if((tmp[0] & 0x10) == 0x00) //1110zzzz - 0 on fourth bit
   {
      if(((tmp[1] & 0xc0) == 0x80) && ((tmp[2] & 0xC0) == 0x80))
      {
         //        10yyyyyy  10xxxxxx
         ret = ((uint32_t)tmp[0] & 0x0F);
         ret <<= 6; //shift by y
         ret |= ((uint32_t)tmp[1] & 0x3F);
         ret <<= 6; //shift by x
         ret |= ((uint32_t)tmp[2] & 0x3F);
         offset+=3;
      }
      else
      {
         ret = 0xFFFFFFFF;
      }
   }
   else if((tmp[0] & 0x08) == 0x00) //11110uuu - 0 on fifth bit
   {
      if(((tmp[1] & 0xc0) == 0x80) && ((tmp[2] & 0xc0) == 0x80) && ((tmp[3] & 0xc0) == 0x80))
      {
         ret = ((uint32_t)tmp[0] & 0x0F);
         ret <<= 6; //shift by z
         ret |= ((uint32_t)tmp[1] & 0x3F);
         ret <<= 6; //shift by y
         ret |= ((uint32_t)tmp[2] & 0x3F);
         ret <<= 6; //shift by x
         ret |= ((uint32_t)tmp[3] & 0x3F);
         offset+=4;
      }
   }
   m_pos += offset;

   return ret;
}

UTF_Common &UTF8::operator=(const UTF8 &other)
{
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
      m_string = 0;
   }

   m_string = CSCONVERTER_NEW(char[other.get_string_size() + 1]);
   m_size = other.get_string_size();

   memcpy(m_string, other.get_string(), other.get_string_size());
   m_string[get_string_size()] = 0;

   return *this;
}

UTF_Common &UTF8::operator+=(UTF8 &other)
      {
   char *creator = 0;
   const char *tmp = other.get_string();
   uint32_t size = other.get_plain_string_size();

   creator = CSCONVERTER_NEW(char[size+m_size+1]);
   if(m_string)
   {
      memcpy(creator, m_string, m_size);
      CSCONVERTER_DELETE2(m_string, char);
   }
   if(tmp)
   {
      memcpy(&creator[m_size], tmp, size);
   }

   m_size += size;
   creator[m_size] = 0;

   m_string = creator;

   return *this;
      }

UTF_Common &UTF8::operator+=(UTF_Common &other)
      {
   return append(other);
      }

UTF_Common &UTF8::operator=(const UTF_Common &other)
{
   return assign(other);
}

bool UTF8::set_string(const char *str, const size_t& size)
{
   size_t alloc_size = size;
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
      m_string = 0;
   }

   if(size != 0)
   {
      if(str[size-1] != 0)
      {
         alloc_size += 1;
      }

      m_string = CSCONVERTER_NEW(char[alloc_size]);
      memcpy(m_string, str, size);
      m_string[alloc_size-1] = 0;
   }
   m_size = size;
   return true;
}

UTF_Common &UTF8::operator=(const char *str)
{
   size_t size = strlen(str);
   set_string(str, size);

   return *this;
}

UTF_Common &UTF8::operator+=(const char *str)
      {
   add_cstring(str);

   return *this;
      }

#ifdef USE_DLOPEN

extern "C" UTF_Common *generate(SCONV_ENDIANESS endianess)
{
   return new UTF8();
}

extern "C" void *create_library(Common *ptr)
{
   ptr->insert(SCONV_E_UTF_8, "UTF-8", generate);
   return nullptr;
}

extern "C" void assign(Converter_in_if *dst, const char *src)
{
   UTF8 *iso = dynamic_cast<UTF8 *>(dst);
   *iso = src;
}

extern "C" void assign_Common(Converter_in_if *dst, Converter_in_if *src)
{
   UTF8 *iso = dynamic_cast<UTF8 *>(dst);
   UTF_Common *cmn = dynamic_cast<UTF_Common *>(src);
   *iso = *cmn;
}

extern "C" void append(Converter_in_if *dst, const char *src)
{
   UTF8 *iso = dynamic_cast<UTF8 *>(dst);
   *iso += src;
}

extern "C" void append_Common(Converter_in_if *dst, UTF_Common *src)
{
   UTF8 *iso = dynamic_cast<UTF8 *>(dst);
   UTF_Common *cmn = dynamic_cast<UTF_Common *>(src);
   *iso += *cmn;
}

#endif
}

