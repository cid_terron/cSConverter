/*
 * UTF8.h
 *
 *  Created on: Feb 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef CONVERTER_UTF8_UTF8_H_
#define CONVERTER_UTF8_UTF8_H_

#include "cSConverter/CONVERTER_IN_IF.h"
#include "cSConverter/UTF_COMMON.h"

namespace cSConverter
{

class UTF8 : public UTF_Common
{
public:
    UTF8();
    UTF8(const UTF8 &other);
    UTF8(const UTF_Common &other);
    UTF8(const char *str);
    virtual ~UTF8(){}
    virtual bool validate(const char *txt, const size_t& length) override;
    virtual bool add_cstring(const char *str) override;
    virtual bool add_unicode(uint32_t U) override;
    virtual uint32_t get_next(const char **str) const override;
    virtual uint32_t get_next() override;
    virtual bool set_string(const char *str, const size_t& size) override;

    virtual UTF_Common &operator=(const UTF8 &other);
    virtual UTF_Common &operator=(const UTF_Common &other);

    virtual UTF_Common &operator+=(UTF8 &other);
    virtual UTF_Common &operator+=(UTF_Common &other);

    virtual UTF_Common &operator=(const char *str);
    virtual UTF_Common &operator+=(const char *str);

    virtual const size_t get_plain_string_size(){return m_size;}
    virtual const SCON_UTF get_utf_type(){return SCONV_E_UTF_8;}
};

}

#endif /* CONVERTER_UTF8_UTF8_H_ */
