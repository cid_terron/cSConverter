/*
 * Common.cpp
 *
 *  Created on: Feb 29, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "Common.h"
#include "string.h"

#include "cSConverter/CONVERTER_IN_IF.h"
#include "Alloc_Fkt.h"

#include "cSConverter/UTF_COMMON.h"
#include "cSConverter/UTF8.h"
#include "cSConverter/UTF16.h"
#include "cSConverter/UTF32.h"
#ifndef BUILD_NO_ISO
#include "cSConverter/ISO8859-1.h"
#include "cSConverter/ISO8859-2.h"
#include "cSConverter/ISO8859-3.h"
#include "cSConverter/ISO8859-4.h"
#include "cSConverter/ISO8859-5.h"
#include "cSConverter/ISO8859-6.h"
#include "cSConverter/ISO8859-7.h"
#include "cSConverter/ISO8859-8.h"
#include "cSConverter/ISO8859-9.h"
#include "cSConverter/ISO8859-10.h"
#include "cSConverter/ISO8859-11.h"
#include "cSConverter/ISO8859-13.h"
#include "cSConverter/ISO8859-14.h"
#include "cSConverter/ISO8859-15.h"
#include "cSConverter/ISO8859-16.h"
#endif

namespace cSConverter
{

const char *Common::m_utf[SCONV_UTF_MAX+1] =
{
      "UTF-8",
      "UTF-16",
      "UTF-32",
#ifndef BUILD_NO_ISO
      "ISO8859-1",
      "ISO8859-2",
      "ISO8859-3",
      "ISO8859-4",
      "ISO8859-5",
      "ISO8859-6",
      "ISO8859-7",
      "ISO8859-8",
      "ISO8859-9",
      "ISO8859-10",
      "ISO8859-11",
      "ISO8859-13",
      "ISO8859-14",
      "ISO8859-15",
      "ISO8859-16",
#endif
      "UTF_MAX"
};

const char *Common::m_end[SCONV_ENDIANESS_MAX+1] =
{
      "SINGLE",
      "BIG",
      "LITTLE",
      "ENDIANESS_MAX"
};

Common::Common()
{
}

UTF_Common *Common::generate(SCON_UTF type, SCONV_ENDIANESS endianess)
{
   UTF_Common *ret = 0;
   switch(type)
   {
      case SCONV_E_UTF_8:
      {
         ret = CSCONVERTER_NEW(UTF8);
         break;
      }
      case SCONV_E_UTF_16:
      {
         ret = CSCONVERTER_NEW2(UTF16,endianess);
         break;
      }
      case SCONV_E_UTF_32:
      {
         ret = CSCONVERTER_NEW2(UTF32,endianess);
         break;
      }
#ifndef BUILD_NO_ISO
      case SCONV_E_ISO8859_1:
      {
         ret = CSCONVERTER_NEW(ISO8859_1);
         break;
      }
      case SCONV_E_ISO8859_2:
      {
         ret = CSCONVERTER_NEW(ISO8859_2);
         break;
      }
      case SCONV_E_ISO8859_3:
      {
         ret = CSCONVERTER_NEW(ISO8859_3);
         break;
      }
      case SCONV_E_ISO8859_4:
      {
         ret = CSCONVERTER_NEW(ISO8859_4);
         break;
      }
      case SCONV_E_ISO8859_5:
      {
         ret = CSCONVERTER_NEW(ISO8859_5);
         break;
      }
      case SCONV_E_ISO8859_6:
      {
         ret = CSCONVERTER_NEW(ISO8859_6);
         break;
      }
      case SCONV_E_ISO8859_7:
      {
         ret = CSCONVERTER_NEW(ISO8859_7);
         break;
      }
      case SCONV_E_ISO8859_8:
      {
         ret = CSCONVERTER_NEW(ISO8859_8);
         break;
      }
      case SCONV_E_ISO8859_9:
      {
         ret = CSCONVERTER_NEW(ISO8859_9);
         break;
      }
      case SCONV_E_ISO8859_10:
      {
         ret = CSCONVERTER_NEW(ISO8859_10);
         break;
      }
      case SCONV_E_ISO8859_11:
      {
         ret = CSCONVERTER_NEW(ISO8859_11);
         break;
      }
      case SCONV_E_ISO8859_13:
      {
         ret = CSCONVERTER_NEW(ISO8859_13);
         break;
      }
      case SCONV_E_ISO8859_14:
      {
         ret = CSCONVERTER_NEW(ISO8859_14);
         break;
      }
      case SCONV_E_ISO8859_15:
      {
         ret = CSCONVERTER_NEW(ISO8859_15);
         break;
      }
      case SCONV_E_ISO8859_16:
      {
         ret = CSCONVERTER_NEW(ISO8859_16);
         break;
      }
#endif
      case SCONV_UTF_MAX:
      {

         break;
      }
   }

   return ret;
}

UTF_Common *Common::generate(const char *name, const char *endianess)
{
   SCON_UTF utf = SCONV_E_UTF_8;
   SCONV_ENDIANESS end = SCONV_E_SINGLE;
   uint8_t i = 0;

   for(i = 0; i < SCONV_UTF_MAX; i++)
   {
      if(strcasecmp(m_utf[i], name) == 0)
      {
         utf = (SCON_UTF)i;
         break;
      }
   }

   for(i = 0; i < SCONV_ENDIANESS_MAX; i++)
   {
      if(strcasecmp(m_end[i], endianess) == 0)
      {
         end = (SCONV_ENDIANESS)i;
         break;
      }
   }
   return generate(utf, end);
}

}

#include "errno.h"

extern "C" Converter_in_if *cSConverterByName(const char *endianess, const char *utf)
{
   return cSConverter::Common::generate(utf, endianess);
}

extern "C" Converter_in_if *cSConverterByType(SCONV_ENDIANESS endianess, SCON_UTF utf)
{
   return cSConverter::Common::generate(utf, endianess);
}

extern "C" void cSConverter_set_text(Converter_in_if *dest, const char *str, const size_t size)
{
   cSConverter_complete_append_unicode(dest);
   if(dest && str)
   {
      dest->set_string(str, size);
   }
   else
   {
      errno = EFAULT;
   }
}

extern "C" void cSConverter_add_text(Converter_in_if *dest, const char *str, const size_t size)
{
   cSConverter::UTF_Common *ptr = dynamic_cast<cSConverter::UTF_Common *>(dest);
   if(ptr && str)
   {
      ptr->add_string(str, size);
   }
}

extern "C" void cSConverter_append_unicode(Converter_in_if *dest, uint32_t uc)
{
   cSConverter::UTF_Common *common = dynamic_cast<cSConverter::UTF_Common *>(dest);
   if(common)
   {
      common->add_unicode(uc);
   }
}

extern "C" void cSConverter_complete_append_unicode(Converter_in_if *dest)
{
   cSConverter::UTF_Common *common = dynamic_cast<cSConverter::UTF_Common *>(dest);
   if(common)
   {
      common->complete_add();
   }
}

extern "C" void cSConverter_append_utf(Converter_in_if *dest, Converter_in_if *src)
{
   cSConverter::UTF_Common *dst_common = dynamic_cast<cSConverter::UTF_Common *>(dest);
   cSConverter::UTF_Common *src_common = dynamic_cast<cSConverter::UTF_Common *>(src);


   if(dst_common && src_common)
   {
      //    *dst_common += *src_common;
      switch(dst_common->get_utf_type())
      {
         case SCONV_E_UTF_8:
         {
            cSConverter::UTF8 *utf = dynamic_cast<cSConverter::UTF8 *>(dst_common);
            if(utf)
            {
               *utf += *src_common;
            }
            break;
         }
         case SCONV_E_UTF_16:
         {
            cSConverter::UTF16 *utf = dynamic_cast<cSConverter::UTF16 *>(dst_common);
            if(utf)
            {
               *utf += *src_common;
            }
            break;
         }
         case SCONV_E_UTF_32:
         {
            cSConverter::UTF32 *utf = dynamic_cast<cSConverter::UTF32 *>(dst_common);
            if(utf)
            {
               (*utf)+=(*src_common);
            }
            break;
         }
#ifndef BUILD_NO_ISO
         case SCONV_E_ISO8859_1:
         {
            cSConverter::ISO8859_1 *iso = dynamic_cast<cSConverter::ISO8859_1 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_2:
         {
            cSConverter::ISO8859_2 *iso = dynamic_cast<cSConverter::ISO8859_2 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_3:
         {
            cSConverter::ISO8859_3 *iso = dynamic_cast<cSConverter::ISO8859_3 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_4:
         {
            cSConverter::ISO8859_4 *iso = dynamic_cast<cSConverter::ISO8859_4 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_5:
         {
            cSConverter::ISO8859_5 *iso = dynamic_cast<cSConverter::ISO8859_5 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_6:
         {
            cSConverter::ISO8859_6 *iso = dynamic_cast<cSConverter::ISO8859_6 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_7:
         {
            cSConverter::ISO8859_7 *iso = dynamic_cast<cSConverter::ISO8859_7 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_8:
         {
            cSConverter::ISO8859_8 *iso = dynamic_cast<cSConverter::ISO8859_8 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_9:
         {
            cSConverter::ISO8859_9 *iso = dynamic_cast<cSConverter::ISO8859_9 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_10:
         {
            cSConverter::ISO8859_10 *iso = dynamic_cast<cSConverter::ISO8859_10 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_11:
         {
            cSConverter::ISO8859_11 *iso = dynamic_cast<cSConverter::ISO8859_11 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_13:
         {
            cSConverter::ISO8859_13 *iso = dynamic_cast<cSConverter::ISO8859_13 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_14:
         {
            cSConverter::ISO8859_14 *iso = dynamic_cast<cSConverter::ISO8859_14 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_15:
         {
            cSConverter::ISO8859_15 *iso = dynamic_cast<cSConverter::ISO8859_15 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_16:
         {
            cSConverter::ISO8859_16 *iso = dynamic_cast<cSConverter::ISO8859_16 *>(dst_common);
            if(iso)
            {
               *iso += *src_common;
            }
            break;
         }
#endif
         case SCONV_UTF_MAX:
         {
            errno = EFAULT;
            break;
         }
      }
   }
   else
   {
      errno = EFAULT;
   }

}

extern "C" uint32_t cSConverter_get_unicode(Converter_in_if *utf, const char **str)
{
   uint32_t ret = 0;
   cSConverter::UTF_Common *common = dynamic_cast<cSConverter::UTF_Common *>(utf);
   if(common)
   {
      ret = common->get_next(str);
   }

   return ret;
}

extern "C" void cSConverter_assign_utf(Converter_in_if *dest, Converter_in_if *src)
{
   cSConverter::UTF_Common *dst_common = dynamic_cast<cSConverter::UTF_Common *>(dest);
   cSConverter::UTF_Common *src_common = dynamic_cast<cSConverter::UTF_Common *>(src);

   if(dst_common && src_common)
   {

      switch(dst_common->get_utf_type())
      {
         case SCONV_E_UTF_8:
         {
            cSConverter::UTF8 *utf = dynamic_cast<cSConverter::UTF8 *>(dst_common);
            if(utf)
            {
               *utf = *src_common;
            }
            break;
         }
         case SCONV_E_UTF_16:
         {
            cSConverter::UTF16*utf = dynamic_cast<cSConverter::UTF16 *>(dst_common);
            if(utf)
            {
               *utf = *src_common;
            }
            break;
         }
         case SCONV_E_UTF_32:
         {
            cSConverter::UTF32 *utf = dynamic_cast<cSConverter::UTF32 *>(dst_common);
            if(utf)
            {
               *utf = *src_common;
            }
            break;
         }
#ifndef BUILD_NO_ISO
         case SCONV_E_ISO8859_1:
         {
            cSConverter::ISO8859_1 *iso = dynamic_cast<cSConverter::ISO8859_1 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_2:
         {
            cSConverter::ISO8859_2 *iso = dynamic_cast<cSConverter::ISO8859_2 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_3:
         {
            cSConverter::ISO8859_3 *iso = dynamic_cast<cSConverter::ISO8859_3 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_4:
         {
            cSConverter::ISO8859_4 *iso = dynamic_cast<cSConverter::ISO8859_4 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_5:
         {
            cSConverter::ISO8859_5 *iso = dynamic_cast<cSConverter::ISO8859_5 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_6:
         {
            cSConverter::ISO8859_6 *iso = dynamic_cast<cSConverter::ISO8859_6 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_7:
         {
            cSConverter::ISO8859_7 *iso = dynamic_cast<cSConverter::ISO8859_7 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_8:
         {
            cSConverter::ISO8859_8 *iso = dynamic_cast<cSConverter::ISO8859_8 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_9:
         {
            cSConverter::ISO8859_9 *iso = dynamic_cast<cSConverter::ISO8859_9 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_10:
         {
            cSConverter::ISO8859_10 *iso = dynamic_cast<cSConverter::ISO8859_10 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_11:
         {
            cSConverter::ISO8859_11 *iso = dynamic_cast<cSConverter::ISO8859_11 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_13:
         {
            cSConverter::ISO8859_13 *iso = dynamic_cast<cSConverter::ISO8859_13 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_14:
         {
            cSConverter::ISO8859_14 *iso = dynamic_cast<cSConverter::ISO8859_14 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_15:
         {
            cSConverter::ISO8859_15 *iso = dynamic_cast<cSConverter::ISO8859_15 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
         case SCONV_E_ISO8859_16:
         {
            cSConverter::ISO8859_16 *iso = dynamic_cast<cSConverter::ISO8859_16 *>(dst_common);
            if(iso)
            {
               *iso = *src_common;
            }
            break;
         }
#endif
         case SCONV_UTF_MAX:
         {
            errno = EFAULT;
            break;
         }
      }
   }
   else
   {
      errno = EFAULT;
   }

}


extern "C" void cSConverter_get_content(const char **str, size_t *size, Converter_in_if *src)
{
   cSConverter_complete_append_unicode(src);

   if(src)
   {
      size_t tsize = src->get_string_size();
      const char *tstr = src->get_string();

      *size = tsize;
      *str = tstr;
   }
}

extern "C" void cSConverterFree(Converter_in_if *converter)
{
   if(converter)
   {
//      converter->~Converter_in_if();
//      cSConverter::Alloc_Fkt::dealloc_function(converter);
      CSCONVERTER_DELETE(converter, Converter_in_if);
   }
}

extern "C" uint8_t compare(Converter_in_if *a, Converter_in_if *b)
{
   cSConverter::UTF_Common *tmp_a = dynamic_cast<cSConverter::UTF_Common *>(a);
   cSConverter::UTF_Common *tmp_b = dynamic_cast<cSConverter::UTF_Common *>(b);
   uint8_t ret = 0;

   cSConverter_complete_append_unicode(a);
   cSConverter_complete_append_unicode(b);

   if(tmp_a && tmp_b)
   {
      if(*tmp_a == *tmp_b)
      {
         ret = 1;
      }
   }

   return ret;
}

extern "C" int32_t cSConverter_compare(Converter_in_if *utf1, Converter_in_if *utf2)
{
   int32_t ret = 0xff;

   cSConverter::UTF_Common *a = dynamic_cast<cSConverter::UTF_Common *>(utf1);
   cSConverter::UTF_Common *b = dynamic_cast<cSConverter::UTF_Common *>(utf2);

   cSConverter_complete_append_unicode(a);
   cSConverter_complete_append_unicode(b);

   if(a && b)
   {
      if(*a == *b)
      {
         ret = 0;
      }
      else if(*a < *b)
      {
         ret = -1;
      }
      else if(*a > *b)
      {
         ret = 1;
      }
   }

   return ret;
}

extern "C" void cSConverter_set_alloc_function(M_ALLOC_FTYPE *fkt)
{
   cSConverter::Alloc_Fkt::cSConverter_set_alloc_function(fkt);
}

extern "C" void cSConverter_set_dealloc_function(M_DEALLOC_FTYPE *fkt)
{
   cSConverter::Alloc_Fkt::cSConverter_set_dealloc_function(fkt);
}

