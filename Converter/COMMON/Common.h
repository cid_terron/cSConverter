/*
 * Common.h
 *
 *  Created on: Feb 29, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef CONVERTER_COMMON_COMMON_H_
#define CONVERTER_COMMON_COMMON_H_

#include "cSConverter/UTF_COMMON.h"
#include "cSConverter/cSConverter.h"

namespace cSConverter
{
class Common
{
    static const char *m_utf[SCONV_UTF_MAX+1];
    static const char *m_end[SCONV_ENDIANESS_MAX+1];
  public:
    Common();
    static UTF_Common *generate(SCON_UTF type, SCONV_ENDIANESS endianess);
    static UTF_Common *generate(const char *name, const char *endianess);
};
}


#endif /* CONVERTER_COMMON_COMMON_H_ */
