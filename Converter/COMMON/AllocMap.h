/*
 * AllocMap.h
 *
 *  Created on: Apr 10, 2016
 *      Author: Dominik Frizel
 */

#ifndef CONVERTER_COMMON_ALLOCCSCONVERTER_MAP_H_
#define CONVERTER_COMMON_ALLOCCSCONVERTER_MAP_H_

#include "cSConverter/Alloc_Fkt.h"

namespace cSConverter
{

template <typename key, typename values>
class AllocMap
{
public:
   class iterator_it
   {
      iterator_it *next;
      iterator_it *prev;
      AllocMap *parent;
   public:
      key first;
      values second;

      iterator_it operator++()
      {
         iterator_it it;
         if(next)
         {
            it->next = next->next;
            it->prev = next->prev;
            it->first = next->first;
            it->second = next->second;
         }
         else
         {
            it->next = 0;
            it->prev = 0;
            it->first = 0;
            it->second = 0;
         }

         return *this;
      }
      friend AllocMap;
   };

   typedef iterator_it *iterator;

   iterator_it m_first;

   iterator_it begin()
   {
      iterator ret = &m_first;
      return ret;
   }

   AllocMap()
   {
      m_first.next = 0;
      m_first.prev = 0;
   }

   values &operator[](key key_val)
   {
      iterator_it *cur = &m_first;
      iterator_it *old = &m_first;
      while(cur)
      {
         if(cur->first < key_val)
         {
            old = cur;
            cur = cur->next;
         }
         else if(cur->first > key_val )
         {
            iterator_it *it = CSCONVERTER_NEW(iterator_it);
            it->first = key_val;
            memset(&it->second, 0, sizeof(values));
            it->prev = cur->prev;
            it->next = cur;
            cur->prev = it;
            if(it->prev)
            {
               it->prev->next = it;
            }
            cur = it;
            break;
         }
         else
         {
            //cur == key_val
            break;
         }
      }

      if(cur == 0)
      {
         old->next = CSCONVERTER_NEW(iterator_it);
         cur = old->next;
         cur->next = 0;
         cur->prev = old;
         cur->first = key_val;
         cur->second = 0;
      }


      return cur->second;
   }

   iterator find(const key &key_val)
   {
      iterator_it *cur = &m_first;
      while(cur)
      {
         if(cur->first < key_val)
         {
            cur = cur->next;
         }
         else if(cur->first > key_val)
         {
            cur = 0;
         }
         else
         {
            break;
         }
      }

      return cur;
   }

   iterator end()
   {
      return 0;
   }
};

}

#endif /* CONVERTER_COMMON_ALLOCCSCONVERTER_MAP_H_ */
