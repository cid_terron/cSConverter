/*
 * Alloc_String.cpp
 *
 *  Created on: Apr 5, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/


#include "Alloc_String.h"
#include "cSConverter/Alloc_Fkt.h"

#include "string.h"

uint32_t Alloc_String::m_alloc_string_id = 0;

//Allows up to 4 Byte of 0 for UTF32
static char empty[4] = {0,0,0,0};

#ifndef TARGET_PATH
#define TARGET_PATH "/tmp/cSCONVERTER"
#endif

namespace cSConverter
{

Alloc_String::Alloc_String()
{
   m_string_size = 0;
   m_field_size = 0;
   sprintf(m_filename, "%s.%u", TARGET_PATH, m_alloc_string_id++);
   m_content = empty;
   printf("%i %s\n", __LINE__, m_filename);
   m_fp = fopen(m_filename, "w");
   printf("%i %s %p\n", __LINE__, m_filename, m_fp);
   if(m_fp)
   {
     fclose(m_fp);
   }
   m_end = true;
   m_change = 0;
}

Alloc_String::~Alloc_String()
{
   printf("%i\n", __LINE__);
   if(m_content && m_content != empty)
   {
      CSCONVERTER_DELETE2(m_content, char);
   }
}

Alloc_String::Alloc_String(const char *str)
{
   printf("%i\n", __LINE__);
   m_field_size = 0;
   sprintf(m_filename, "%s.%u", TARGET_PATH, m_alloc_string_id++);
   m_fp = fopen(m_filename, "w");
   if(m_fp)
   {
      fclose(m_fp);
   }
   m_end = true;
   m_change = 0;
   m_content = empty;
   m_string_size = 0;
   if(str)
   {
      uint32_t size = strlen(str);
      for(uint32_t i = 0; i < size; i++)
      {
         push_back(str[i]);
      }
   }
}

void Alloc_String::clear()
{
   if(m_content)
   {
      memset(m_content, 0, m_field_size);
   }
   m_string_size = 0;
}

size_t Alloc_String::size()
{
   return m_string_size;
}


const char *Alloc_String::c_str()
{
   m_fp = fopen(m_filename, "r");
   if(m_string_size > m_field_size)
   {
      if(m_content != empty)
      {
         CSCONVERTER_DELETE2(m_content, char);
      }

      m_content = CSCONVERTER_NEW(char[m_string_size]);
      m_field_size = m_string_size;
   }

   if(m_change)
   {
      rewind(m_fp);
      fread(m_content, 1, m_string_size, m_fp);
      m_end = false;
   }
   fclose(m_fp);
   return m_content;
}

void Alloc_String::push_back(uint8_t c)
{
   m_fp = fopen(m_filename, "a");
   if(m_end == false)
   {
      fseek(m_fp, 0, SEEK_END);
      m_end = true;
   }
   m_change = true;
   fputc(c, m_fp);
   m_string_size++;
   fclose(m_fp);

}
}
