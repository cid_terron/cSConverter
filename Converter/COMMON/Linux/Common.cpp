/*
 * Common.cpp
 *
 *  Created on: Mar 27, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/


#include "Common.h"
#include "string.h"

#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>

#include <sys/stat.h>
#include <fcntl.h>

#include <dlfcn.h>

//#include "../Alloc_Fkt.h"

#include "cSConverter/CONVERTER_IN_IF.h"
#include "cSConverter/UTF_COMMON.h"

#ifndef INSTALL_DIR
#define INSTALL_DIR "/usr/local/"
#endif

#ifndef LIBRARY_DIR
#define LIBRARY_DIR "/usr/local/lib"
#endif

namespace cSConverter
{

Common *Common::initialize = nullptr;

const char *Common::m_end[SCONV_ENDIANESS_MAX+1] =
{
      "SINGLE",
      "BIG",
      "LITTLE",
      "ENDIANESS_MAX"
};


CSCONVERTER_MAP<CSCONVERTER_STRING, uint32_t> Common::m_name_map;
CSCONVERTER_MAP<uint32_t, void *> Common::m_generator;
uint32_t Common::m_current_id = 0x100;

Common::Common()
{
   DIR *dir = nullptr;
   struct dirent *entry = nullptr;
   const char *install_dir = INSTALL_DIR "/" LIBRARY_DIR "/";
   const char *library_dir = LIBRARY_DIR "/";
   const char *search_dir;


   if(LIBRARY_DIR[0] == '/')
   {
      search_dir = library_dir;
   }
   else
   {
      search_dir = install_dir;
   }


   if(search_dir)
   {
      int fd = open(search_dir, 0);

      if(fd > -1)
      {
         dir = fdopendir(fd);
      }

      if(dir != nullptr)
      {
         while ((entry = readdir(dir)) != nullptr)
         {
            if(strstr(entry->d_name, "_dynamic.so\0"))
            {
               char tmp[1024];
               snprintf(tmp, 1024, "%s%s", search_dir, entry->d_name);
               void *dlptr = dlopen(tmp, RTLD_NOW);
               if(dlptr)
               {
                  m_ptr = dlptr;
                  void *sym = dlsym(dlptr, "create_library");
                  if(sym)
                  {
                     INITIALIZER *init = reinterpret_cast<INITIALIZER *>(sym);
                     init(this);
                  }
               }
            }
         }
         closedir(dir);
      }

      close(fd);
   }
}


uint32_t Common::get_current_id()
{
   return m_current_id;
}

void Common::insert(uint32_t id, const char *str,  GENERATOR generator)
{
   if(m_current_id >= id)
   {
      m_current_id = id+1;
   }

   m_name_map[str] = id;
   //TODO: make this better, the pointer is somehow bad
   m_generator[id] = m_ptr;
}

extern "C" int32_t cSConverter_compare(Converter_in_if *utf1, Converter_in_if *utf2)
{
   int32_t ret = 0xff;

   cSConverter::UTF_Common *a = dynamic_cast<cSConverter::UTF_Common *>(utf1);
   cSConverter::UTF_Common *b = dynamic_cast<cSConverter::UTF_Common *>(utf2);

   if(a && b)
   {
      if(*a == *b)
      {
         ret = 0;
      }
      else if(*a < *b)
      {
         ret = -1;
      }
      else if(*a > *b)
      {
         ret = 1;
      }
   }

   return ret;
}

UTF_Common *Common::generate(uint32_t type, SCONV_ENDIANESS endianess)
{
   if(initialize == nullptr)
   {
      initialize = new Common();
   }

   UTF_Common *ret = 0;
   CSCONVERTER_MAP<uint32_t, void *>::iterator it = m_generator.find(type);
   if(it != m_generator.end())
   {
      void *sym = dlsym(it->second, "generate");

      if(sym)
      {
         GENERATOR *generator = reinterpret_cast<GENERATOR *>(sym);
         ret = generator(endianess);
      }
   }

   return ret;
}

UTF_Common *Common::generate(const char *name, const char *endianess)
{
   if(initialize == nullptr)
   {
      initialize = new Common();
   }

   SCON_UTF utf = SCONV_E_UTF_8;
   SCONV_ENDIANESS end = SCONV_E_SINGLE;
   uint8_t i = 0;

   CSCONVERTER_MAP<CSCONVERTER_STRING, uint32_t>::iterator it = m_name_map.find(name);

   if(it != m_name_map.end())
   {
      utf = it->second;

      for(i = 0; i < SCONV_ENDIANESS_MAX; i++)
      {
         if(strcasecmp(m_end[i], endianess) == 0)
         {
            end = (SCONV_ENDIANESS)i;
            break;
         }
      }
      return generate(utf, end);
   }
   else
   {
      printf("Failed %s\n", name);
   }
   return nullptr;
}

}


extern "C" Converter_in_if *cSConverterByName(const char *endianess, const char *utf)
{
   return cSConverter::Common::generate(utf, endianess);
}

extern "C" Converter_in_if *cSConverterByType(SCONV_ENDIANESS endianess, SCON_UTF utf)
{
   return cSConverter::Common::generate(utf, endianess);
}

extern "C" void cSConverter_set_text(Converter_in_if *dest, const char *str, const size_t size)
{
   if(dest && str)
   {
      dest->set_string(str, size);
   }
   else
   {
      errno = EFAULT;
   }
}

extern "C" void cSConverter_add_text(Converter_in_if *dest, const char *str, const size_t size)
{
   cSConverter::UTF_Common *ptr = dynamic_cast<cSConverter::UTF_Common *>(dest);
   if(ptr && str)
   {
      ptr->add_string(str, size);
   }
}

void cSConverter::Common::cSConverter_assign_utf(Converter_in_if *dest, Converter_in_if *src)
{
   cSConverter::UTF_Common *common = dynamic_cast<cSConverter::UTF_Common *>(dest);
   cSConverter::UTF_Common *common2 = dynamic_cast<cSConverter::UTF_Common *>(src);
   if(common)
   {
      CSCONVERTER_MAP<uint32_t, void *>::iterator it = m_generator.find(common->get_utf_type());
      printf("%i .. %i\n", common->get_utf_type(), common2->get_utf_type());
      if(it != m_generator.end())
      {
         DBL_COMMON* call = reinterpret_cast<DBL_COMMON *>(dlsym(it->second, "assign_Common"));
         if(call)
         {
            call(dest, src);
         }
      }
   }
}

extern "C" void cSConverter_assign_utf(Converter_in_if *dest, Converter_in_if *src)
{
   cSConverter::Common::cSConverter_assign_utf(dest, src);
}

void cSConverter::Common::cSConverter_append_utf(Converter_in_if *dest, Converter_in_if *src)
{
   UTF_Common *common = dynamic_cast<UTF_Common *>(dest);
   if(common)
   {
      CSCONVERTER_MAP<uint32_t, void *>::iterator it = m_generator.find(common->get_utf_type());
      if(it != m_generator.end())
      {
         DBL_COMMON* call = reinterpret_cast<DBL_COMMON *>(dlsym(it->second, "append_Common"));
         if(call)
         {
            call(dest, src);
         }
      }
   }
}

extern "C" void cSConverter_append_unicode(Converter_in_if *dest, uint32_t uc)
{
   cSConverter::UTF_Common *common = dynamic_cast<cSConverter::UTF_Common *>(dest);
   if(common)
   {
      common->add_unicode(uc);
   }
}

extern "C" void cSConverter_complete_append_unicode(Converter_in_if *dest)
{
   cSConverter::UTF_Common *common = dynamic_cast<cSConverter::UTF_Common *>(dest);
   if(common)
   {
      common->complete_add();
   }
}

extern "C" uint32_t cSConverter_get_unicode(Converter_in_if *utf, const char **str)
{
   uint32_t ret = 0;
   cSConverter::UTF_Common *common = dynamic_cast<cSConverter::UTF_Common *>(utf);
   if(common)
   {
      ret = common->get_next(str);
   }

   return ret;
}

extern "C" uint8_t compare(Converter_in_if *a, Converter_in_if *b)
{
   cSConverter::UTF_Common *tmp_a = dynamic_cast<cSConverter::UTF_Common *>(a);
   cSConverter::UTF_Common *tmp_b = dynamic_cast<cSConverter::UTF_Common *>(b);
   uint8_t ret = 0;

   if(tmp_a && tmp_b)
   {
      if(*tmp_a == *tmp_b)
      {
         ret = 1;
      }
   }

   return ret;
}

extern "C" void cSConverter_append_utf(Converter_in_if *dest, Converter_in_if *src)
{
   cSConverter::Common::cSConverter_append_utf(dest, src);
}

extern "C" void cSConverter_get_content(const char **str, size_t *size, Converter_in_if *src)
{
   if(src)
   {
      *size = src->get_string_size();
      *str = src->get_string();
   }
}

extern "C" void cSConverterFree(Converter_in_if *converter)
{
   if(converter)
   {
      converter->~Converter_in_if();
      cSConverter::Alloc_Fkt::dealloc_function(converter);
   }
}

extern "C" void cSConverter_set_alloc_function(M_ALLOC_FTYPE *fkt)
{
   cSConverter::Alloc_Fkt::cSConverter_set_alloc_function(fkt);
}

extern "C" void cSConverter_set_dealloc_function(M_DEALLOC_FTYPE *fkt)
{
   cSConverter::Alloc_Fkt::cSConverter_set_dealloc_function(fkt);
}
