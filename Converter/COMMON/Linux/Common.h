/*
 * Common.h
 *
 *  Created on: Feb 29, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef CONVERTER_COMMON_COMMON_H_
#define CONVERTER_COMMON_COMMON_H_

#include "cSConverter/UTF_COMMON.h"
#include "cSConverter/cSConverter.h"
#include "cSConverter/Alloc_Fkt.h"

#include CSCONVERTER_MAP_INC
#include CSCONVERTER_STRING_INC

namespace cSConverter
{

class Common
{
   typedef void *(INITIALIZER)(Common *);
   typedef UTF_Common *(GENERATOR)(SCONV_ENDIANESS end);
   typedef void (DBL_COMMON)(Converter_in_if *dst, Converter_in_if *src);
   static CSCONVERTER_MAP<CSCONVERTER_STRING, uint32_t> m_name_map;
   static CSCONVERTER_MAP<uint32_t, void *> m_generator;
   static uint32_t m_current_id;
   static const char *m_end[SCONV_ENDIANESS_MAX+1];
   static Common *initialize;
   void *m_ptr;

public:
   Common();
   uint32_t get_current_id();
   void insert(uint32_t id, const char *str, GENERATOR *generator);
   static UTF_Common *generate(uint32_t type, SCONV_ENDIANESS endianess);
   static UTF_Common *generate(const char *name, const char *endianess);
   static void cSConverter_assign_utf(Converter_in_if *dest, Converter_in_if *src);
   static void cSConverter_append_utf(Converter_in_if *dest, Converter_in_if *src);
};
}


#endif /* CONVERTER_COMMON_COMMON_H_ */
