/*
 * Alloc_Fkt.cpp
 *
 *  Created on: Mar 1, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "Alloc_Fkt.h"
#include "stdlib.h"

namespace cSConverter
{

Alloc_Fkt::M_ALLOC_FTYPE *Alloc_Fkt::m_fkt = Alloc_Fkt::default_alloc_function;
Alloc_Fkt::M_DEALLOC_FTYPE *Alloc_Fkt::m_free_fkt = Alloc_Fkt::default_dealloc_function;

void *Alloc_Fkt::alloc_function(size_t size)
{
   return m_fkt(size);
}

void *Alloc_Fkt::default_alloc_function(size_t size)
{
   return malloc(size);
}

void Alloc_Fkt::cSConverter_set_alloc_function(M_ALLOC_FTYPE *fkt)
{
   m_fkt = fkt;
   if(m_free_fkt == default_dealloc_function)
   {
      m_free_fkt = 0;
   }
}

void Alloc_Fkt::dealloc_function(void *ptr)
{
   if(m_free_fkt)
   {
      m_free_fkt(ptr);
   }
}

void Alloc_Fkt::cSConverter_set_dealloc_function(M_DEALLOC_FTYPE *fkt)
{
   m_free_fkt = fkt;
}

void Alloc_Fkt::default_dealloc_function(void *ptr)
{
   free(ptr);
}

}
