/*
 * Alloc_Fkt.h
 *
 *  Created on: Mar 1, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef CONVERTER_COMMON_ALLOC_FKT_H_
#define CONVERTER_COMMON_ALLOC_FKT_H_

#include "stdint.h"
#include "stdlib.h"
#include <memory>

namespace cSConverter
{

class Alloc_Fkt
{
private:
   typedef void* (M_ALLOC_FTYPE)(size_t size);
   typedef void (M_DEALLOC_FTYPE)(void *ptr);

   static M_ALLOC_FTYPE *m_fkt;
   static M_DEALLOC_FTYPE *m_free_fkt;

   static void *default_alloc_function(size_t size);

   static void default_dealloc_function(void *ptr);

public:
   static void *alloc_function(size_t size);
   static void cSConverter_set_alloc_function(M_ALLOC_FTYPE *fkt);

   static void dealloc_function(void *ptr);
   static void cSConverter_set_dealloc_function(M_DEALLOC_FTYPE *fkt);
};

#ifdef PREALLOC_MEMORY
#define CSCONVERTER_NEW(TYPE) \
   new(Alloc_Fkt::alloc_function(sizeof(TYPE))) TYPE()

#define CSCONVERTER_DELETE(NAME, TYPE) \
   NAME->~TYPE(); Alloc_Fkt::dealloc_function(NAME)

#define CSCONVERTER_NEW2(TYPE, PARAM) \
   new(Alloc_Fkt::alloc_function(sizeof(TYPE))) TYPE(PARAM)

#define CSCONVERTER_DELETE2(NAME, TYPE) \
   Alloc_Fkt::dealloc_function(NAME)

#define CSCONVERTER_MAP AllocMap
#define CSCONVERTER_STRING Alloc_String
#define CSCONVERTER_MAP_INC "cSConverter/AllocMap.h"
#define CSCONVERTER_STRING_INC "cSConverter/Alloc_String.h"

#else
#define CSCONVERTER_NEW(TYPE) \
   new TYPE()

#define CSCONVERTER_DELETE(NAME, TYPE) \
   delete NAME

#define CSCONVERTER_NEW2(TYPE, PARAM) \
   new TYPE(PARAM)

#define CSCONVERTER_DELETE2(NAME, TYPE) \
   delete [] NAME;

#define CSCONVERTER_STRING std::string

#define CSCONVERTER_STRING_INC "string"

#define CSCONVERTER_MAP_INC "map"
#define CSCONVERTER_MAP std::map


#endif //PREALLOC_MEMORY

}

#endif /* CONVERTER_COMMON_ALLOC_FKT_H_ */
