/*
 * Alloc_String.h
 *
 *  Created on: Apr 5, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/


#ifndef CONVERTER_COMMON_ALLOC_CSCONVERTER_STRING_H_
#define CONVERTER_COMMON_ALLOC_CSCONVERTER_STRING_H_

#include "stdint.h"
#include "stdio.h"
#include "string.h"

namespace cSConverter
{

class Alloc_String
{
   static uint32_t m_alloc_string_id;

   size_t m_string_size; //current size of data
   size_t m_field_size; //maximum size of data
   char m_filename[100];
   char *m_content; //ptr to data
   FILE *m_fp; //ptr to file
   bool m_end;
   bool m_change;

public:
   Alloc_String();
   Alloc_String(const char *str);
   virtual ~Alloc_String();
   virtual void clear();
   virtual size_t size();
   virtual const char *c_str();
   virtual const char *str() const {return m_content;}
   virtual void push_back(uint8_t c);

   friend bool operator<(const Alloc_String& foo,const Alloc_String& foo1) {  return (strcmp(foo.str(), foo1.str()) < 0); }
   friend bool operator>(const Alloc_String& foo,const Alloc_String& foo1) { return (strcmp(foo.str(), foo1.str()) > 0);}
};

}


#endif /* CONVERTER_COMMON_ALLOC_CSCONVERTER_STRING_H_ */
