/*
 * UTF_COMMON.h
 *
 *  Created on: Feb 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef CONVERTER_UTF_UTF_COMMON_H_
#define CONVERTER_UTF_UTF_COMMON_H_

#include "stdint.h"

#include "cSConverter/cSConverter.h"

#include "cSConverter/CONVERTER_IN_IF.h"
#include "cSConverter/Alloc_Fkt.h"

#include CSCONVERTER_STRING_INC

namespace cSConverter
{

class UTF_Common : public Converter_in_if
{
  protected:

    typedef struct
    {
      uint8_t m_min;
      uint8_t m_max;
    } UTF_PAIRS;

    typedef struct
    {
       uint8_t m_cnt;
       UTF_PAIRS *m_pairs;
    } UTF_CONTENT;

    size_t m_size;
    char *m_string;
    size_t m_pos;

private:
    CSCONVERTER_STRING m_tmp;
    UTF_CONTENT *m_pairs;
    uint32_t m_pair_pos;
    uint32_t m_pair_count;

protected:
    void pushElem(uint8_t elem);
    void add_pair(UTF_PAIRS *pairs, uint32_t count);
    bool iterate(uint32_t i, bool &quit, const char *str);

  protected:
    SCONV_ENDIANESS m_endianess;

  protected:
    UTF_Common &assign(UTF_Common &other);
    UTF_Common &assign(const UTF_Common &other);
    UTF_Common &append(UTF_Common &other);

  public:
    UTF_Common(SCONV_ENDIANESS sEndian);
    virtual ~UTF_Common();
    virtual bool add_unicode(uint32_t U) = 0;
    virtual void complete_add();

    void begin_parse(uint32_t size);
    virtual uint32_t get_next() = 0;
    virtual uint32_t get_next(const char **str) const = 0;

    virtual const size_t get_plain_string_size() = 0;

    UTF_Common &operator+=(const char *str);
    bool operator==(const UTF_Common &) const;
    bool operator<(const UTF_Common &) const;
    bool operator>(const UTF_Common &) const;
    virtual UTF_Common &operator=(UTF_Common &other);
    bool equals(const UTF_Common &other) const;

    const SCONV_ENDIANESS get_endianess() const {return m_endianess;}

    const char *get_string() const;
    const size_t get_string_size() const {return m_size;}
    virtual size_t get_text_length(){return m_size;}
    virtual const SCON_UTF get_utf_type() = 0;
    virtual bool validate(const char *txt, const size_t& length) = 0;
    virtual bool add_cstring(const char *str) = 0;
    virtual bool set_string(const char *str, const size_t& size) = 0;
    virtual void add_string(const char *str, const size_t &size);
    virtual void create_pairs(uint32_t amount);
    virtual size_t nullsize(){return 1;}
};
}


#endif /* CONVERTER_UTF_UTF_COMMON_H_ */
