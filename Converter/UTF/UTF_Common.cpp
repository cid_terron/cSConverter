/*
 * UTF_Common.cpp
 *
 *  Created on: Feb 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "UTF_COMMON.h"

#include "string.h"

namespace cSConverter
{
UTF_Common::UTF_Common(SCONV_ENDIANESS sEndian)
{
  m_endianess = sEndian;
  m_string = 0;
  m_pos = 0;
  m_size = 0;
  m_pairs = 0;
  m_pair_pos = 0;
  m_pair_count = 0;
}

UTF_Common::~UTF_Common()
{
  if(m_string)
  {
     CSCONVERTER_DELETE2(m_string, char);
  }
  if(m_pairs)
  {
     for(uint32_t i = 0; i < m_pair_count; i++)
     {
        if(m_pairs[i].m_pairs)
        {
           CSCONVERTER_DELETE2(m_pairs[i].m_pairs, UTF_PAIRS);
        }
     }
     CSCONVERTER_DELETE2(m_pairs, UTF_CONTENT);
  }
}

void UTF_Common::pushElem(uint8_t elem)
{
   m_tmp.push_back(elem);
}

void UTF_Common::add_pair(UTF_PAIRS *pairs, uint32_t count)
{
  UTF_CONTENT tmp;

  tmp.m_cnt = count;
  tmp.m_pairs = CSCONVERTER_NEW(UTF_PAIRS[count]);
  memcpy(tmp.m_pairs, pairs, sizeof(UTF_PAIRS) * count);

  if(m_pair_pos < m_pair_count)
  {
     m_pairs[m_pair_pos] = tmp;
     m_pair_pos++;
  }
  else
  {
     fprintf(stderr, "Illegal Insert into m_pairs\n");
  }
}

bool UTF_Common::iterate(uint32_t i, bool &quit, const char *str)
{
  bool ret = false;
  for(uint32_t pos = 0; pos < m_pair_count ; pos++, i++)
  {
     UTF_CONTENT &content = m_pairs[pos];
    for(uint8_t i = 0; i < content.m_cnt; i++)
    {
      uint8_t c = (uint8_t) str[i]; //cast for HEX comparison
      UTF_PAIRS &pair = content.m_pairs[i];
      if(c >= pair.m_min && c <= pair.m_max)
      {
        quit = true;
        ret = true;
      }
      else
      {
        ret = false;
        break;
      }
    }

    if(quit)
    {
      break;
    }
  }

  return ret;
}

UTF_Common &UTF_Common::operator+=(const char *str)
{
  add_cstring(str);
  return *this;
}

void UTF_Common::begin_parse(uint32_t size)
{
  m_pos = 0;
}


UTF_Common &UTF_Common::append(UTF_Common &other)
{
  uint32_t tmp = 0;
  other.begin_parse(0);
  m_tmp.clear();
  do
  {
    tmp = other.get_next();
    add_unicode(tmp);
  }
  while(tmp != 0 && tmp != 0xFFFFFFFF);

  complete_add();

  return *this;
}

UTF_Common &UTF_Common::assign(const UTF_Common &other)
{
   if(m_string)
   {
     m_size = 0;
     CSCONVERTER_DELETE2(m_string, char);
   }

   uint32_t tmp = 0;
   m_tmp.clear();
   const char *str = other.get_string();
   do
   {
     tmp = other.get_next(&str);
     add_unicode(tmp);
   }
   while(tmp != 0 && tmp != 0xFFFFFFFF);

   m_string = CSCONVERTER_NEW(char[m_tmp.size() + 4]);
   memset(&m_string[m_tmp.size()], 0, 4);

   m_size = m_tmp.size();
   memcpy(m_string, m_tmp.c_str(), m_tmp.size());

   return *this;
}

UTF_Common &UTF_Common::assign(UTF_Common &other)
{
  if(m_string)
  {
    m_size = 0;
    CSCONVERTER_DELETE2(m_string, char);
  }

  uint32_t tmp = 0;
  m_tmp.clear();
  other.begin_parse(0);
  do
  {
    tmp = other.get_next();
    add_unicode(tmp);
  }
  while(tmp != 0 && tmp != 0xFFFFFFFF);

  m_string = CSCONVERTER_NEW(char[m_tmp.size()]);
  m_size = m_tmp.size();
  memcpy(m_string, m_tmp.c_str(), m_tmp.size());

  return *this;
}

UTF_Common &UTF_Common::operator=(UTF_Common &other)
{
  assign(other);
  return *this;
}

bool UTF_Common::equals(const UTF_Common &other) const
{
   bool ret = true;
   const char *me_txt = get_string();
   const char *ot_txt = other.get_string();
   uint32_t var = 0;
   while(me_txt && ot_txt && (var = get_next(&me_txt)) != 0)
   {
      if(var != other.get_next(&ot_txt))
      {
         ret = false;
         break;
      }
   }

   if(ret == true)
   {
      if(other.get_next(&ot_txt) != 0)
      {
         ret = false;
      }
   }
//   begin_parse(0);
//   other.begin_parse(0);
//   uint32_t var = 0;
//   while(((var = get_next()) != 0))
//   {
//      if(var != other.get_next())
//      {
//         ret = false;
//         break;
//      }
//   }
//
//   if(ret == true)
//   {
//      if(other.get_next() != 0)
//      {
//         ret = false;
//      }
//   }

   return ret;
}

void UTF_Common::create_pairs(uint32_t amount)
{
   if(m_pairs)
   {
      CSCONVERTER_DELETE2(m_pairs, UTF_CONTENT);
      m_pairs = 0;
      m_pair_count = 0;
   }

   m_pairs = CSCONVERTER_NEW(UTF_CONTENT[amount]);
   m_pair_count = amount;
}

bool UTF_Common::operator==(const UTF_Common &other) const
{
   return equals(other);
}

bool UTF_Common::operator<(const UTF_Common &other) const
{
   const char *str_a = get_string();
   const char *str_b = other.get_string();
   bool ret = false;
   uint32_t var = 0;
   uint32_t varb = 0;

   do
   {
      var = get_next(&str_a);
      varb = other.get_next(&str_b);

      if(var < varb)
      {
         ret = true;
         break;
      }
      else if(var > varb)
      {
         break;
      }
   }
   while(var != 0);

   return ret;
}

bool UTF_Common::operator>(const UTF_Common &other) const
{
   bool ret = false;
   const char *str_a = get_string();
   const char *str_b = other.get_string();
   uint32_t var = 0;
   uint32_t varb = 0;

   do
   {
      var = get_next(&str_a);
      varb = other.get_next(&str_b);
      if(var > varb)
      {
         ret = true;
         break;
      }
      else if(var < varb)
      {
         break;
      }
   }
   while(var != 0);

   return ret;
}

void UTF_Common::complete_add()
{
   if(m_tmp.size() > 0)
   {
      char *current = CSCONVERTER_NEW(char[get_plain_string_size() + m_tmp.size()+nullsize()]);
      memset(&current[get_plain_string_size() + m_tmp.size()], 0, 4);
      memcpy(current, m_string, get_plain_string_size());
      memcpy(&current[get_plain_string_size()], m_tmp.c_str(), m_tmp.size());

      if(m_string)
      {
         CSCONVERTER_DELETE2(m_string, char);
         m_string = 0;
      }

      m_string = current;
      m_size = get_plain_string_size() + m_tmp.size();

      m_tmp.clear();
   }
}

const char *UTF_Common::get_string() const
{
   return m_string;
}

void UTF_Common::add_string(const char *str, const size_t &size)
{
   size_t n_size = m_size + size;
   if(m_size == 0)
   {
      set_string(str, size);
   }
   else if(str)
   {
      char *tmp = 0;
      tmp = CSCONVERTER_NEW(char[n_size + nullsize()]);
      memcpy(tmp, m_string, m_size);
      memcpy(&tmp[m_size], str, size);
      memset(&tmp[n_size], 0, nullsize());
      CSCONVERTER_DELETE(m_string, char);
      m_string = tmp;
      m_size = n_size;
   }
}
}
