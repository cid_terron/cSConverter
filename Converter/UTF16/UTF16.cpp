/*
 * UTF16.cpp
 *
 *  Created on: Feb 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
 */


#include "UTF16.h"
#include "UTF8/UTF8.h"

#include "cSConverter/Common.h"

#include "string.h"

namespace cSConverter
{

UTF16::UTF16(SCONV_ENDIANESS sEndian) : UTF_Common(sEndian)
{
   create_pairs(3);

   if(sEndian == SCONV_E_BIG)
   {
      UTF_PAIRS tmp[4] = {{0, 0xd7}, {0x0, 0xff}};
      UTF_PAIRS tmp2[4] = {{0xe0, 0xff}, {0x0, 0xff}};
      UTF_PAIRS tmp3[4] = {{0xd8, 0xdf}, {0x0, 0xff}, {0xdc, 0xdf}, {0x00, 0xff}};

      add_pair(tmp, 2);
      add_pair(tmp2, 2);
      add_pair(tmp3, 4);
   }
   else if(sEndian == SCONV_E_LITTLE)
   {
      UTF_PAIRS tmp[4] =  {{0x0, 0xff}, {0, 0xd7}};
      UTF_PAIRS tmp2[4] = {{0x0, 0xff}, {0xe0, 0xff}};
      UTF_PAIRS tmp3[4] = {{0x0, 0xff}, {0xd8, 0xdf}, {0x00, 0xff}, {0xdc, 0xdf}};

      add_pair(tmp, 2);
      add_pair(tmp2, 2);
      add_pair(tmp3, 4);
   }
}

UTF16::UTF16(const UTF_Common &other) : UTF_Common(other.get_endianess())
{
   create_pairs(3);

   SCONV_ENDIANESS sEndian = other.get_endianess();

   if(sEndian == SCONV_E_BIG)
   {
      UTF_PAIRS tmp[4] = {{0, 0xd7}, {0x0, 0xff}};
      UTF_PAIRS tmp2[4] = {{0xe0, 0xff}, {0x0, 0xff}};
      UTF_PAIRS tmp3[4] = {{0xd8, 0xdf}, {0x0, 0xff}, {0xdc, 0xdf}, {0x00, 0xff}};

      add_pair(tmp, 2);
      add_pair(tmp2, 2);
      add_pair(tmp3, 4);
   }
   else if(sEndian == SCONV_E_LITTLE)
   {
      UTF_PAIRS tmp[4] =  {{0x0, 0xff}, {0, 0xd7}};
      UTF_PAIRS tmp2[4] = {{0x0, 0xff}, {0xe0, 0xff}};
      UTF_PAIRS tmp3[4] = {{0x0, 0xff}, {0xd8, 0xdf}, {0x00, 0xff}, {0xdc, 0xdf}};

      add_pair(tmp, 2);
      add_pair(tmp2, 2);
      add_pair(tmp3, 4);
   }

   assign(other);
}

UTF16::UTF16(const UTF16 &other) : UTF_Common(other.get_endianess())
{
   create_pairs(3);

   SCONV_ENDIANESS sEndian = other.get_endianess();

   if(sEndian == SCONV_E_BIG)
   {
      UTF_PAIRS tmp[4] = {{0, 0xd7}, {0x0, 0xff}};
      UTF_PAIRS tmp2[4] = {{0xe0, 0xff}, {0x0, 0xff}};
      UTF_PAIRS tmp3[4] = {{0xd8, 0xdf}, {0x0, 0xff}, {0xdc, 0xdf}, {0x00, 0xff}};

      add_pair(tmp, 2);
      add_pair(tmp2, 2);
      add_pair(tmp3, 4);
   }
   else if(sEndian == SCONV_E_LITTLE)
   {
      UTF_PAIRS tmp[4] =  {{0x0, 0xff}, {0, 0xd7}};
      UTF_PAIRS tmp2[4] = {{0x0, 0xff}, {0xe0, 0xff}};
      UTF_PAIRS tmp3[4] = {{0x0, 0xff}, {0xd8, 0xdf}, {0x00, 0xff}, {0xdc, 0xdf}};

      add_pair(tmp, 2);
      add_pair(tmp2, 2);
      add_pair(tmp3, 4);
   }

   assign(other);
}

bool UTF16::validate(const char *str, const size_t& size)
{
   bool ret = false;
   bool quit = false;

   if((size%2 == 0))
   {
      {
         for(size_t i = 0; (i < size); i++)
         {
            ret = iterate(i, quit, str);
            if(quit || (ret != true))
            {
               break;
            }
         }
      }
   }

   return ret;
}

bool UTF16::add_unicode(uint32_t U)
{
   //  Codepoint range                 Unicode scalar value    Code units (binary)
   //  U+0000..U+D7FF, U+E000..U+EFFF  00000xxxxxxxxxxxxxxxx   xxxxxxxxxxxxxxxx
   //  U+10000..U+10FFFF               Uuuuuxxxxxxyyyyyyyyyy   110110wwwwxxxxxx 110111yyyyyyyyyy (where uuuuu = wwww + 1)

   uint8_t FIRST = 0;
   uint8_t SECOND = 0;
   uint8_t var[2];

   bool ret = true;

   if(m_endianess == SCONV_E_LITTLE)
   {
      FIRST = 0;
      SECOND = 1;
   }
   else if(m_endianess == SCONV_E_BIG)
   {
      FIRST = 1;
      SECOND = 0;
   }

   if((U <= 0xD7FF) || (U >= 0xE00 && U <= 0xEFFF))
   {
      uint16_t tmp = (U & 0xFFFF);
      var[FIRST] = (tmp & 0xFF);
      tmp >>= 8;
      var[SECOND] = (tmp & 0xFF);
      pushElem(var[0]);
      pushElem(var[1]);
   }
   else if(U >= 0x10000 && U <= 0x10FFFF )
   {
      //    000Uuuuu.xxxxxxyy.yyyyyyyy
      //1101-11yy.yyyy-yyyy
      uint32_t tmp = U;
      uint8_t tmpx = 0;
      uint8_t tmpu = 0;
      var[FIRST] = (tmp & 0xFF);
      tmp >>= 8; //remove first 8 y
      var[SECOND] = (tmp & 0x3);
      var[SECOND] |= 0xDC;
      tmp >>= 2; //remove last 2 y;
      pushElem(var[0]);
      pushElem(var[1]);
      //110110ww.wwxx-xxxx
      //000Uuu-uuxx-xxxx
      tmpx = (tmp & 0x3F);
      tmp >>= 6;
      //000U-uuuu
      tmpu = (tmp & 0x1F);
      tmpu--; //now it is wwww;
      var[FIRST] = (tmpu & 0x3);
      var[FIRST] <<= 6;
      var[FIRST] |= (tmpx & 0x3F);
      tmpu >>= 2;
      //1101-10ww
      var[SECOND] = (tmpu & 0x3);
      var[SECOND] |= 0xd8;

      pushElem(var[0]);
      pushElem(var[1]);
   }
   else
   {
      ret = false;
   }

   return ret;
}

#ifdef USE_DLOPEN
bool UTF16::add_cstring(const char *str)
{
   UTF_Common *common = Common::generate(SCONV_E_UTF_8, SCONV_E_SINGLE);
   //There is NO static cast because of DLOPEN !
   UTF8 *tmp = static_cast<UTF8*>(common);
   if(tmp)
   {
      tmp->add_cstring(str);
   }
   append(*tmp);
   cSConverterFree(tmp);
   return true;
}
#else
bool UTF16::add_cstring(const char *str)
{
   UTF8 tmp;
   tmp.add_cstring(str);
   append(tmp);

   return true;
}
#endif

uint32_t UTF16::get_next(const char **str) const
{
   const char *tmp = *str;
   uint32_t ret = 0;
   uint32_t offset = 0;
   uint8_t FIRST = 0;
   uint8_t SECOND = 1;

   if(tmp == 0)
   {
      return 0;
   }

   //  Codepoint range                 Unicode scalar value    (binary) Code units (binary)
   //  U+0000..U+D7FF, U+E000..U+EFFF  00000xxxxxxxxxxxxxxxx   xxxxxxxxxxxxxxxx
   //  U+10000..U+10FFFF               Uuuuuxxxxxxyyyyyyyyyy   110110ww.wwxxxxxx 110111yy.yyyyyyyy (where uuuuu = wwww + 1
   if(m_endianess == SCONV_E_BIG)
   {
      FIRST = 0;
      SECOND = 1;
   }
   else
   {
      FIRST = 1;
      SECOND = 0;
   }

   if((tmp[FIRST] >= 0 && tmp[FIRST] <= 0xD7) || (tmp[FIRST] >= 0xE0 && tmp[FIRST] <= 0xEF))
   {
      ret = tmp[FIRST];
      ret <<= 8;
      ret |= ((uint32_t)tmp[SECOND] & 0x000000FF);
      offset += 2;
   }
   else if((tmp[FIRST] & 0xfc) == 0xd8) //1101.10ww
   {
      if((tmp[FIRST + 2] & 0xfc) == 0xdc) //1101.11yy
      {
         uint8_t tmp2 = tmp[SECOND]; //wwxx.xxxx
         uint8_t uu = tmp[FIRST] & 0x03; //1101.10ww
         uu <<= 2;
         tmp2 >>= 6; //0000.00ww
         uu |= (tmp2 & 0x3);
         uu++; //wwww +1 => uuuuu
         ret = uu;
         ret <<= 6; // move it by bits for x
         //wwxx.xxxx
         ret |= (((uint32_t)tmp[FIRST]) & 0x0000003F);
         ret <<= 2; //move for first part of y
         ret |= ((uint32_t)tmp[FIRST+2] & 0x00000002); //1101.11yy
         //yyyyyyyy
         ret <<= 8; //move for rest of y
         ret |= ((uint32_t)tmp[SECOND+2] & 0x000000FF);
         offset += 4;
      }
   }
   else
   {
      ret = 0xffffffff; //Something is wrong
   }


   tmp += offset;
   *str = tmp;

   return ret;
}

uint32_t UTF16::get_next()
{
   uint8_t *tmp = (uint8_t *)&m_string[m_pos];
   uint32_t ret = 0;
   uint32_t offset = 0;
   uint8_t FIRST = 0;
   uint8_t SECOND = 1;
   //  Codepoint range                 Unicode scalar value    (binary) Code units (binary)
   //  U+0000..U+D7FF, U+E000..U+EFFF  00000xxxxxxxxxxxxxxxx   xxxxxxxxxxxxxxxx
   //  U+10000..U+10FFFF               Uuuuuxxxxxxyyyyyyyyyy   110110ww.wwxxxxxx 110111yy.yyyyyyyy (where uuuuu = wwww + 1
   if(m_endianess == SCONV_E_BIG)
   {
      FIRST = 0;
      SECOND = 1;
   }
   else
   {
      FIRST = 1;
      SECOND = 0;
   }

   if((tmp[FIRST] >= 0 && tmp[FIRST] <= 0xD7) || (tmp[FIRST] >= 0xE0 && tmp[FIRST] <= 0xEF))
   {
      ret = tmp[FIRST];
      ret <<= 8;
      ret |= ((uint32_t)tmp[SECOND] & 0x000000FF);
      offset += 2;
   }
   else if((tmp[FIRST] & 0xfc) == 0xd8) //1101.10ww
   {
      if((tmp[FIRST + 2] & 0xfc) == 0xdc) //1101.11yy
      {
         uint8_t tmp2 = tmp[SECOND]; //wwxx.xxxx
         uint8_t uu = tmp[FIRST] & 0x03; //1101.10ww
         uu <<= 2;
         tmp2 >>= 6; //0000.00ww
         uu |= (tmp2 & 0x3);
         uu++; //wwww +1 => uuuuu
         ret = uu;
         ret <<= 6; // move it by bits for x
         //wwxx.xxxx
         ret |= (((uint32_t)tmp[FIRST]) & 0x0000003F);
         ret <<= 2; //move for first part of y
         ret |= ((uint32_t)tmp[FIRST+2] & 0x00000002); //1101.11yy
         //yyyyyyyy
         ret <<= 8; //move for rest of y
         ret |= ((uint32_t)tmp[SECOND+2] & 0x000000FF);
         offset += 4;
      }
   }
   else
   {
      ret = 0xffffffff; //Something is wrong
   }


   m_pos += offset;
   return ret;
}


UTF_Common &UTF16::operator=(UTF16 &other)
{
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
      m_string = 0;
   }

   m_string = CSCONVERTER_NEW(char[other.get_string_size()]);

   if(m_endianess == other.get_endianess())
   {
      memcpy(m_string, other.get_string(), other.get_string_size()+1);
   }
   else
   {
      const char *tmp = other.get_string();

      for(size_t i = 0; i < other.get_string_size(); i+=2)
      {
         m_string[i+1] = tmp[i];
         m_string[i] = tmp[i+1];
      }
      m_size = other.get_string_size();
   }

   return *this;
}

UTF_Common &UTF16::operator+=(UTF16 &other)
      {
   m_size = get_plain_string_size();
   if(m_endianess == other.get_endianess())
   {
      char *creator = 0;
      const char *tmp = other.get_string();
      const size_t& size = other.get_string_size();

      creator = CSCONVERTER_NEW(char[size+m_size+1]);
      if(m_string)
      {
         memcpy(creator, m_string, m_size);
         CSCONVERTER_DELETE2(m_string, char);
      }
      if(tmp)
      {
         memcpy(&creator[m_size], tmp, size);
      }

      m_size += size;
      creator[m_size] = 0;

      m_string = creator;

      return *this;
   }
   else
   {
      return append(other);
   }
      }

UTF_Common &UTF16::operator+=(UTF_Common &other)
      {
   //  m_size = get_plain_string_size();
   return append(other);
      }

UTF_Common &UTF16::operator=(UTF_Common &other)
{
   return assign(other);
}

bool UTF16::set_string(const char *str, const size_t& size)
{
   size_t alloc_size = size;
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
   }

   if(str[size-1] != 0 || str[size-2] != 0)
   {
      alloc_size += 2;
   }

   m_string = CSCONVERTER_NEW(char[alloc_size]);
   memcpy(m_string, str, size);
   if(alloc_size != size)
   {
      memset(&m_string[size], 0, alloc_size - size);
   }
   m_size = alloc_size;

   return true;
}

UTF_Common &UTF16::operator+=(const char *str)
      {
   add_cstring(str);
   return *this;
      }

UTF_Common &UTF16::operator=(const char *str)
{
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
      m_string = 0;
      m_size = 0;
   }
   add_cstring(str);

   return *this;
}

const uint16_t *UTF16::get_w16string()
{
   return reinterpret_cast<uint16_t *>(m_string);
}

#ifdef USE_DLOPEN

extern "C" UTF_Common *generate(SCONV_ENDIANESS endianess)
{
   return new UTF16(endianess);
}

extern "C" void *create_library(Common *ptr)
{
   ptr->insert(SCONV_E_UTF_16, "UTF_16", generate);
   return nullptr;
}

extern "C" void assign(Converter_in_if *dst, const char *src)
{
   UTF16 *iso = dynamic_cast<UTF16 *>(dst);
   *iso = src;
}

extern "C" void assign_Common(Converter_in_if *dst, Converter_in_if *src)
{
   UTF16 *iso = dynamic_cast<UTF16 *>(dst);
   UTF_Common *cmn = dynamic_cast<UTF_Common *>(src);
   *iso = *cmn;
}

extern "C" void append(Converter_in_if *dst, const char *src)
{
   UTF16 *iso = dynamic_cast<UTF16 *>(dst);
   *iso += src;
}

extern "C" void append_Common(Converter_in_if *dst, UTF_Common *src)
{
   UTF16 *iso = dynamic_cast<UTF16 *>(dst);
   UTF_Common *cmn = dynamic_cast<UTF_Common *>(src);
   *iso += *cmn;
}

#endif

}
