/*
 * ISO8859.h
 *
 *  Created on: Mar 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef CONVERTER_ISO8859_ISO8859_H_
#define CONVERTER_ISO8859_ISO8859_H_

#include "cSConverter/UTF_COMMON.h"

namespace cSConverter
{

class ISO8859 : public UTF_Common
{
private:
public:
   ISO8859(SCON_UTF utf);
   bool add_unicode(uint32_t U) = 0;
   virtual uint32_t get_next() = 0;

   virtual const size_t get_plain_string_size() final;
   virtual const SCON_UTF get_utf_type() = 0;
   virtual bool validate(const char *txt, const size_t& length) final;

   virtual bool add_cstring(const char *str) final;
   virtual bool set_string(const char *str, const size_t& size) final;
};

}


#endif /* CONVERTER_ISO8859_ISO8859_H_ */
