/*
 * ISO8859.cpp
 *
 *  Created on: Mar 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/


#include "cSConverter/ISO8859.h"
#include "cSConverter/UTF8.h"
#include "string.h"

#include "cSConverter/UTF_COMMON.h"
#include "cSConverter/Common.h"

namespace cSConverter
{

ISO8859::ISO8859(SCON_UTF utf) : UTF_Common(SCONV_E_SINGLE)
{
}


const size_t ISO8859::get_plain_string_size()
{
   return m_size;
}


bool ISO8859::validate(const char *txt, const size_t& length)
{
   //not valid
   return true;
}

#ifdef USE_DLOPEN

bool ISO8859::add_cstring(const char *str)
{
   UTF_Common *common = Common::generate(SCONV_E_UTF_8, SCONV_E_SINGLE);
   //There is NO static cast because of DLOPEN !
   UTF8 *tmp = static_cast<UTF8*>(common);
   if(tmp)
   {
      tmp->add_cstring(str);
   }
   append(*tmp);
   cSConverterFree(tmp);

   return true;
}

#else
bool ISO8859::add_cstring(const char *str)
{
   UTF8 tmp;
   tmp.add_cstring(str);
   append(tmp);

   return true;
}
#endif

bool ISO8859::set_string(const char *str, const size_t& size)
{
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
   }

   m_string = CSCONVERTER_NEW(char[size+1]);
   memcpy(m_string, str, size);

   return true;
}

}

