/*
 * cSConverter.h
 *
 *  Created on: Feb 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef INC_CSCONVERTER_H_
#define INC_CSCONVERTER_H_

#include "stddef.h"
#include "stdint.h"

#ifdef __cplusplus
#include "cSConverter/CONVERTER_IN_IF.h"
#else
typedef void Converter_in_if;
#endif


typedef enum
{
  SCONV_E_SINGLE,
  SCONV_E_BIG,
  SCONV_E_LITTLE,
  SCONV_ENDIANESS_MAX
} SCONV_ENDIANESS;

//A convenience Macro for gcc with
//other compilers there probably are other tricks
#if __BYTE_ORDER__ == __LITTLE_ENDIAN
#define SYSTEM_ENDIANESS SCONV_E_LITTLE
#elif __BYTE_ORDER__ == __LITTLE_ENDIAN
#define SYSTEM_ENDIANESS SCONV_E_BIG
#endif

#ifndef USE_DLOPEN
typedef enum
{
  SCONV_E_UTF_8,
  SCONV_E_UTF_16,
  SCONV_E_UTF_32,
#ifndef BUILD_NO_ISO
  SCONV_E_ISO8859_1,
  SCONV_E_ISO8859_2,
  SCONV_E_ISO8859_3,
  SCONV_E_ISO8859_4,
  SCONV_E_ISO8859_5,
  SCONV_E_ISO8859_6,
  SCONV_E_ISO8859_7,
  SCONV_E_ISO8859_8,
  SCONV_E_ISO8859_9,
  SCONV_E_ISO8859_10,
  SCONV_E_ISO8859_11,
  SCONV_E_ISO8859_13,
  SCONV_E_ISO8859_14,
  SCONV_E_ISO8859_15,
  SCONV_E_ISO8859_16,
#endif
  SCONV_UTF_MAX
} SCON_UTF;
#else
typedef int SCON_UTF;
#endif

#ifdef __cplusplus
extern "C"
{
#endif

typedef void* (M_ALLOC_FTYPE)(size_t size);
typedef void (M_DEALLOC_FTYPE)(void *ptr);
/**
 * Function by const char *str
 * slower than cSConverterByType, using the same names
 */
extern Converter_in_if *cSConverterByName(const char *endianess, const char *utf);
extern Converter_in_if *cSConverterByType(SCONV_ENDIANESS endianess, SCON_UTF utf);

extern void cSConverterFree(Converter_in_if *converter);

/**
 * @param dest: UTF Container where data is filled into
 * @param str: String in valid UTF configuration for dest
 * @param size: Length of string of str
 */
extern void cSConverter_set_text(Converter_in_if *dest, const char *str, const size_t size);
extern void cSConverter_add_text(Converter_in_if *dest, const char *str, const size_t size);
extern void cSConverter_append_utf(Converter_in_if *dest, Converter_in_if *src);
extern void cSConverter_assign_utf(Converter_in_if *dest, Converter_in_if *src);

/**
 * In this function a unicode character is added to the temporary
 * store of the Converter.
 * It will only be permanented when cSConverter_complete_append_unicode
 * is called for this Converter!
 *
 * @param dest: UTF Container where data is filled into
 * @param uc: Unicode charater to be added
 */
extern void cSConverter_append_unicode(Converter_in_if *dest, uint32_t uc);
extern void cSConverter_complete_append_unicode(Converter_in_if *dest);

extern uint32_t cSConverter_get_unicode(Converter_in_if *utf, const char **str);

/**
 * @param utf1: UTF Input1
 * @param utf2: UTF Input2
 * return: -1 utf1 < utf2, 0 utf1 = utf2, 1 utf1 > utf2, 0xff ERROR!
 */
extern int32_t cSConverter_compare(Converter_in_if *utf1, Converter_in_if *utf2);

/**
 * @param str: pointer to char *str, will point to content field
 * @param size: pointer to size_t variable
 * @param src: pointer to UTF
 */
extern void cSConverter_get_content(const char **str, size_t *size, Converter_in_if *src);
extern void cSConverter_set_alloc_function(M_ALLOC_FTYPE *fkt);
extern void cSConverter_set_dealloc_function(M_DEALLOC_FTYPE *fkt);

#ifdef __cplusplus
}
#endif


#endif /* INC_CSCONVERTER_H_ */
