/*
 * ISO8859-1.cpp
 *
 *  Created on: Mar 19, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "cSConverter/ISO8859-1.h"
#include "cSConverter/UTF8.h"
#include "cSConverter/Common.h"
#include "string.h"

namespace cSConverter
{

ISO8859_1::ISO8859_1() : ISO8859(SCONV_E_ISO8859_1)
{
}

ISO8859_1::ISO8859_1(const ISO8859_1 &other) : ISO8859(SCONV_E_ISO8859_1)
{
   assign(other);
}

ISO8859_1::ISO8859_1(const UTF_Common &other) : ISO8859(SCONV_E_ISO8859_1)
{
   assign(other);
}

uint32_t ISO8859_1::get_next()
{
   uint32_t ret = 0;

   ret = m_string[m_pos];
   m_pos++;

   return ret;
}

uint32_t ISO8859_1::get_next(const char **str) const
{
   uint32_t ret = 0;

   const char *tmp = *str;
   ret = m_string[m_pos];
   tmp++;
   *str = tmp;

   return ret;
}

bool ISO8859_1::add_unicode(uint32_t U)
{
   bool ret = false;

   if(U & 0xFFFFFF00)
   {
      //INVALID
   }
   else if(U & 0xFF)
   {
      ret = true;
      pushElem(static_cast<uint8_t>(U & 0xFF));
   }

   return ret;
}

UTF_Common &ISO8859_1::operator=(ISO8859_1 &other)
{
   uint32_t size = other.get_string_size();
   m_string = CSCONVERTER_NEW(char[size+1]);
   memcpy(m_string, other.get_string(), size);
   m_string[size] = 0;

   return *this;
}

UTF_Common &ISO8859_1::operator=(UTF_Common &other)
{
   return assign(other);
}

UTF_Common &ISO8859_1::operator+=(ISO8859_1 &other)
{
   uint32_t size = other.get_string_size();
   char *tmp = CSCONVERTER_NEW(char[size + m_size+1]);

   memcpy(tmp, m_string, m_size);
   memcpy(&tmp[m_size], other.get_string(), size);
   m_size += size;
   CSCONVERTER_DELETE2(m_string, char);
   m_string = tmp;
   m_string[m_size] = 0;

   return *this;
}

UTF_Common &ISO8859_1::operator+=(UTF_Common &other)
{
   return append(other);
}

UTF_Common &ISO8859_1::operator+=(const char *str)
{
   add_cstring(str);

   return *this;
}

UTF_Common &ISO8859_1::operator=(const char *str)
{
   if(m_string)
   {
      CSCONVERTER_DELETE2(m_string, char);
      m_string = 0;
      m_size = 0;
   }

   add_cstring(str);

   return *this;
}


#ifdef USE_DLOPEN

extern "C" UTF_Common *generate(SCONV_ENDIANESS endianess)
{
   return new ISO8859_1();
}

extern "C" void *create_library(Common *ptr)
{
   ptr->insert(SCONV_E_ISO8859_1, "ISO8859-1", generate);
   return nullptr;
}

extern "C" void assign(Converter_in_if *dst, const char *src)
{
   ISO8859_1 *iso = dynamic_cast<ISO8859_1 *>(dst);
   *iso = src;
}

extern "C" void assign_Common(Converter_in_if *dst, Converter_in_if *src)
{
   ISO8859_1 *iso = dynamic_cast<ISO8859_1 *>(dst);
   UTF_Common *cmn = dynamic_cast<UTF_Common *>(src);
   *iso = *cmn;
}

extern "C" void append(Converter_in_if *dst, const char *src)
{
   ISO8859_1 *iso = dynamic_cast<ISO8859_1 *>(dst);
   *iso += src;
}

extern "C" void append_Common(Converter_in_if *dst, UTF_Common *src)
{
   ISO8859_1 *iso = dynamic_cast<ISO8859_1 *>(dst);
   UTF_Common *cmn = dynamic_cast<UTF_Common *>(src);
   *iso += *cmn;
}

#endif
}


