/*
 * ISO8859-1.h
 *
 *  Created on: Mar 19, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef CONVERTER_ISO8859_1_ISO8859_1_H_
#define CONVERTER_ISO8859_1_ISO8859_1_H_

#include "cSConverter/ISO8859.h"

namespace cSConverter
{

class ISO8859_1 : public ISO8859
{
public:
   ISO8859_1();
   ISO8859_1(const ISO8859_1 &other);
   ISO8859_1(const UTF_Common &other);
   bool add_unicode(uint32_t U);
   uint32_t get_next();
   uint32_t get_next(const char **str) const;
//   virtual uint32_t get_next();
//
//   virtual const size_t get_plain_string_size();
//   virtual const SCON_UTF &get_utf_type();
//   virtual bool validate(const char *txt, const size_t& length);

   virtual UTF_Common &operator=(ISO8859_1 &other);
   virtual UTF_Common &operator=(UTF_Common &other);
   virtual UTF_Common &operator+=(ISO8859_1 &other);
   virtual UTF_Common &operator+=(UTF_Common &other);
   virtual UTF_Common &operator+=(const char *);
   virtual UTF_Common &operator=(const char *);
   virtual const SCON_UTF get_utf_type(){return SCONV_E_ISO8859_1;}
//
//   virtual bool add_cstring(const char *str);
//   virtual bool set_string(const char *str, const size_t& size);
};
}


#endif /* CONVERTER_ISO8859_1_ISO8859_1_H_ */
