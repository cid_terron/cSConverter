#ifndef CONVERTER_ISO8859_10_ISO8859_10_H_
#define CONVERTER_ISO8859_10_ISO8859_10_H_

#include "cSConverter/ISO8859.h"

namespace cSConverter
{
class ISO8859_10 : public ISO8859
{
public:
   ISO8859_10();
   ISO8859_10(const ISO8859_10 &other);
   ISO8859_10(const UTF_Common &other);
   bool add_unicode(uint32_t U);
   uint32_t get_next(const char **str) const;
   uint32_t get_next();

   virtual UTF_Common &operator=(ISO8859_10 &other);
   virtual UTF_Common &operator=(UTF_Common &other);
   virtual UTF_Common &operator+=(ISO8859_10 &other);
   virtual UTF_Common &operator+=(UTF_Common &other);
   virtual UTF_Common &operator+=(const char *);
   virtual UTF_Common &operator=(const char *);
   virtual const SCON_UTF get_utf_type(){return SCONV_E_ISO8859_10;}
};
}
#endif /* CONVERTER_ISO8859_10_ISO8859_10_H_ */
