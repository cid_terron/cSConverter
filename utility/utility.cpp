/*
 * utility.cpp
 *
 *  Created on: Mar 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"

#include "map"

FILE *input = 0;
FILE *output = 0;
FILE *output_h = 0;
const char *exec_name = "iso_utility";
char class_text[1024] = "class";

char line[1024]="\0";

char header_name[1024]="\0";


typedef void (PARAM_TYPE)(const char *);

PARAM_TYPE *current_input = 0;

void help()
{
   printf("%s ", exec_name);
   printf("[-h || --help] ");
   printf("[<-c || --class> <CLASSNAME>]");
   printf("[<-i || --input> <FILENAME>] ");
   printf("[<-o || --output> <FILENAME>]\n");
   exit(-1);
}

void set_class(const char *name)
{
   snprintf(class_text, 1024, "%s", name);
   current_input = 0;
}

void set_input(const char *filename)
{
   if(input != stdin)
   {
      fprintf(stderr, "Not allowed to set input filename more than once!\n");
      help();
      exit(-1);
   }
   FILE *fp = fopen(filename, "r");
   if(fp)
   {
      input = fp;
   }
   else
   {
      fprintf(stderr, "Could not open input file for reading\n");
      help();
   }
   current_input = 0;
}

void set_output(const char *filename)
{

   if(output != stdout)
   {
      fprintf(stderr, "Not allowed to set input filename more than once!\n");
      help();
   }

   const char *tmp = filename;
   while(tmp)
   {
      const char *tmp2 = strstr(tmp, "/");
      if(tmp2 == 0)
      {
         snprintf(header_name, 1024, "cSConverter/%s.h", tmp);
      }
      else
      {
         tmp2++;
      }
      tmp = tmp2;
   }

   char file[1024];
   snprintf(file, 1024, "%s.cpp" , filename);
   FILE *fp = fopen(file, "w");
   if(fp)
   {
      output = fp;
   }
   else
   {
      fprintf(stderr, "Could not open output file for writing\n");
      help();
   }

   snprintf(file, 1024, "%s.h" , filename);
   fp = fopen(file, "w");
   if(fp)
   {
      output_h = fp;
   }
   else
   {
      fprintf(stderr, "Could not open output file for writing\n");
      help();
   }

   current_input = 0;
   current_input = 0;
}

void parse_params(int argc, char **argv)
{
   exec_name = argv[0];

   for(int i = 1; i < argc; i++) //ignore exec name
   {
      const char *arg = argv[i];
      if(arg[0] == '-' && arg[1] == '-')
      {
         arg+=2;
         if(strcasecmp(arg, "help") == 0)
         {
            help();
         }
         else if(strcasecmp(arg, "input") == 0)
         {
            if(current_input != 0)
            {
               help();
            }
            current_input = set_input;
         }
         else if(strcasecmp(arg, "output") == 0)
         {
            if(current_input != 0)
            {
               help();
            }
            current_input = set_output;
         }
         else if(strcasecmp(arg, "class") == 0)
         {
            if(current_input != 0)
            {
               help();
            }
            current_input = set_class;
         }
         else
         {
            fprintf(stderr, "unknown argument --%s\n", arg);
            help();
         }
      }
      else if(arg[0] == '-')
      {
         arg++;
         if(arg[0] == 'h')
         {
            help();
         }
         else if(arg[0] == 'i')
         {
            if(current_input != 0)
            {
               help();
            }
            current_input = set_input;
         }
         else if(arg[0] == 'o')
         {
            if(current_input != 0)
            {
               help();
            }
            current_input = set_output;
         }
         else if(arg[0] == 'c')
         {
            if(current_input != 0)
            {
               help();
            }
            current_input = set_class;
         }
         else
         {
            fprintf(stderr, "unknown argument -%s\n", arg);
            help();
         }
      }
      else
      {
         if(current_input == 0)
         {
            help();
         }
         current_input(arg);
      }
   }
}

typedef std::map<uint32_t, uint32_t> MAP;
typedef MAP::iterator                MAP_IT;

std::map<uint32_t, uint32_t> match_map;

void read_lines()
{
   while(fgets(line, 1024, input))
   {
      uint32_t a, b;
      char *pos = line;
      char *tmp = 0;
      char *tmp2 = 0;
      while(pos[0] == ' ' || pos[0] == '\t')
      {
         pos++;
      }
      if(pos[0] == 0)
      {
         fprintf(stderr, "Invalid input file no mapping \"0x  0x\" found\n");
         exit(-2);
      }
      if(pos[0] == '#' || pos[0] == '\n')
      {
         continue;
      }
      a = strtol(pos, 0, 0);
      tmp = strstr(pos, " ");
      tmp2 = strstr(pos, "\t");
      if(tmp2 == 0)
      {
         pos = tmp;
      }
      else if(tmp == 0)
      {
         pos = tmp2;
      }
      else if(tmp2 < tmp)
      {
         pos = tmp2;
      }
      else
      {
         pos = tmp;
      }

      if(pos == 0)
      {
         fprintf(stderr, "Invalid input file no mapping \"0x  0x\" found\n");
         exit(-3);
      }

      while(pos[0] == ' ' || pos[0] == '\t')
      {
         pos++;
      }
      if(pos[0] == 0 || pos[0] == '\n')
      {
         fprintf(stderr, "Invalid input file no mapping \"0x  0x\" found\n");
         exit(-2);
      }
      b = strtol(pos, 0, 0);
      match_map[a] = b;
   }
}


void create_header()
{
   fprintf(output_h, "#ifndef CONVERTER_%s_%s_H_\n", class_text, class_text);
   fprintf(output_h, "#define CONVERTER_%s_%s_H_\n\n", class_text, class_text);

   fprintf(output_h, "#include \"cSConverter/ISO8859.h\"\n\n");
   fprintf(output_h, "namespace cSConverter\n");
   fprintf(output_h, "{\n");

   fprintf(output_h, "class %s : public ISO8859\n", class_text);
   fprintf(output_h, "{\n");
   fprintf(output_h, "public:\n");
   fprintf(output_h, "   %s();\n", class_text);
   fprintf(output_h, "   %s(const %s &other);\n", class_text, class_text);
   fprintf(output_h, "   %s(const UTF_Common &other);\n", class_text, class_text);
   fprintf(output_h, "   bool add_unicode(uint32_t U);\n");
   fprintf(output_h, "   uint32_t get_next(const char **str) const;\n");
   fprintf(output_h, "   uint32_t get_next();\n\n");
   fprintf(output_h, "   virtual UTF_Common &operator=(%s &other);\n", class_text);
   fprintf(output_h, "   virtual UTF_Common &operator=(UTF_Common &other);\n");
   fprintf(output_h, "   virtual UTF_Common &operator+=(%s &other);\n", class_text);
   fprintf(output_h, "   virtual UTF_Common &operator+=(UTF_Common &other);\n");
   fprintf(output_h, "   virtual UTF_Common &operator+=(const char *);\n");
   fprintf(output_h, "   virtual UTF_Common &operator=(const char *);\n");
   fprintf(output_h, "   virtual const SCON_UTF get_utf_type(){return SCONV_E_%s;}\n", class_text);
   fprintf(output_h, "};\n");
   fprintf(output_h, "}\n");
   fprintf(output_h, "#endif /* CONVERTER_%s_%s_H_ */\n", class_text, class_text);
}

void create_file()
{
   create_header();
   fprintf(output, "//Automatically created by Utility Tool\n\n");

//   header_name;
   fprintf(output, "#include \"%s\"\n", header_name);
   fprintf(output, "#include \"cSConverter/UTF8.h\"\n\n");

   fprintf(output, "#include \"cSConverter/Alloc_Fkt.h\"\n\n");

   fprintf(output, "#include \"string.h\"\n\n");
   fprintf(output, "namespace cSConverter\n");
   fprintf(output, "{\n");


   fprintf(output, "%s::%s() : ISO8859(SCONV_E_%s)\n", class_text, class_text, class_text);
   fprintf(output, "{\n");
   fprintf(output, "}\n\n");

   fprintf(output, "%s::%s(const %s &other) : ISO8859(SCONV_E_%s)\n", class_text, class_text, class_text, class_text);
   fprintf(output, "{\n");
   fprintf(output, "   assign(other);\n");
   fprintf(output, "}\n\n");

   fprintf(output, "%s::%s(const UTF_Common &other) : ISO8859(SCONV_E_%s)\n", class_text, class_text, class_text);
   fprintf(output, "{\n");
   fprintf(output, "   assign(other);\n");
   fprintf(output, "}\n\n");

   fprintf(output, "UTF_Common &%s::operator=(%s &other)\n", class_text, class_text);
   fprintf(output, "{\n");
   fprintf(output, "   uint32_t size = other.get_string_size();\n");
   fprintf(output, "   m_string = NEW(char[size+1]);\n");
   fprintf(output, "   memcpy(m_string, other.get_string(), size);\n");
   fprintf(output, "   m_string[size] = 0;\n\n");

   fprintf(output, "   return *this;\n");
   fprintf(output, "}\n\n");

   fprintf(output, "UTF_Common &%s::operator=(UTF_Common &other)\n", class_text);
   fprintf(output, "{\n");
   fprintf(output, "   return assign(other);\n");
   fprintf(output, "}\n\n");

   fprintf(output, "UTF_Common &%s::operator+=(%s &other)\n", class_text, class_text);
   fprintf(output, "{\n");
   fprintf(output, "   uint32_t size = other.get_string_size();\n");
   fprintf(output, "   char *tmp = NEW(char[size + m_size+1]);\n\n");

   fprintf(output, "   memcpy(tmp, m_string, m_size);\n");
   fprintf(output, "   memcpy(&tmp[m_size], other.get_string(), size);\n");
   fprintf(output, "   m_size += size;\n");
   fprintf(output, "   DELETE2(m_string, char);\n");
   fprintf(output, "   m_string = tmp;\n");
   fprintf(output, "   m_string[m_size] = 0;\n\n");

   fprintf(output, "   return *this;\n");
   fprintf(output, "}\n\n");

   fprintf(output, "UTF_Common &%s::operator+=(UTF_Common &other)\n", class_text);
   fprintf(output, "{\n");
   fprintf(output, "   return append(other);\n");
   fprintf(output, "}\n\n");

   fprintf(output, "UTF_Common &%s::operator+=(const char *str)\n", class_text);
   fprintf(output, "{\n");
   fprintf(output, "   add_cstring(str);\n");

   fprintf(output, "   return *this;\n");
   fprintf(output, "}\n\n");

   fprintf(output, "UTF_Common &%s::operator=(const char *str)\n", class_text);
   fprintf(output, "{\n");
   fprintf(output, "   if(m_string)\n");
   fprintf(output, "   {\n");
   fprintf(output, "      DELETE2(m_string, char);\n");
   fprintf(output, "      m_string = 0;\n");
   fprintf(output, "      m_size = 0;\n");
   fprintf(output, "   }\n\n");

   fprintf(output, "   add_cstring(str);\n\n");

   fprintf(output, "   return *this;\n");
   fprintf(output, "}\n\n");

   fprintf(output, "uint32_t %s::get_next()\n", class_text);
   fprintf(output, "{\n");
   fprintf(output, "   uint32_t ret = 0;\n");
   fprintf(output, "   ret = m_string[m_pos];\n");
   fprintf(output, "   m_pos++; \n");
   fprintf(output, "   switch(ret)\n");
   fprintf(output, "   {\n");

   for(MAP_IT it = match_map.begin(); it != match_map.end(); it++)
   {
      fprintf(output, "      case 0x%x:\n", it->first);
      fprintf(output, "      {\n");
      fprintf(output, "         ret = 0x%x;\n", it->second);
      fprintf(output, "         break;\n");
      fprintf(output, "      }\n");
   }

   fprintf(output, "      default:\n");
   fprintf(output, "      {\n");
   fprintf(output, "         //Do nothing\n");
   fprintf(output, "      }\n");
   fprintf(output, "   }\n");
   fprintf(output, "   return ret;\n");
   fprintf(output, "}\n\n\n");

   fprintf(output, "uint32_t %s::get_next(const char **str) const\n", class_text);
   fprintf(output, "{\n");
   fprintf(output, "   uint32_t ret = 0;\n");
   fprintf(output, "   const char *tmp = *str;\n");
   fprintf(output, "   ret = m_string[m_pos];\n");
   fprintf(output, "   switch(ret)\n");
   fprintf(output, "   {\n");

   for(MAP_IT it = match_map.begin(); it != match_map.end(); it++)
   {
      fprintf(output, "      case 0x%x:\n", it->first);
      fprintf(output, "      {\n");
      fprintf(output, "         ret = 0x%x;\n", it->second);
      fprintf(output, "         break;\n");
      fprintf(output, "      }\n");
   }

   fprintf(output, "      default:\n");
   fprintf(output, "      {\n");
   fprintf(output, "         //Do nothing\n");
   fprintf(output, "      }\n");
   fprintf(output, "   }\n");
   fprintf(output, "   tmp++;\n");
   fprintf(output, "   *str = tmp;\n");
   fprintf(output, "   return ret;\n");
   fprintf(output, "}\n\n\n");

   fprintf(output, "bool %s::add_unicode(uint32_t U)\n", class_text);
   fprintf(output, "{\n");
   fprintf(output, "   bool ret = true;\n");
   fprintf(output, "   uint8_t var = 0;\n");
   fprintf(output, "   switch(U)\n");
   fprintf(output, "   {\n");
   for(MAP_IT it = match_map.begin(); it != match_map.end(); it++)
   {
      fprintf(output, "      case 0x%x:\n", it->second);
      fprintf(output, "      {\n");
      fprintf(output, "         var = 0x%x;\n", (it->first & 0xff));
      fprintf(output, "         break;\n");
      fprintf(output, "      }\n");
   }
   fprintf(output, "      default:\n");
   fprintf(output, "      {\n");
   fprintf(output, "         ret = false;\n");
   fprintf(output, "      }\n");
   fprintf(output, "   }\n");
   fprintf(output, "   if(ret)\n");
   fprintf(output, "   {\n");
   fprintf(output, "      pushElem(var);\n");
   fprintf(output, "   }\n");
   fprintf(output, "   return ret;\n");
   fprintf(output, "}\n");

   fprintf(output, "}\n");

}

int main(int argc, char **argv)
{
   input = stdin;
   output = stdout;
   parse_params(argc, argv);
   read_lines();
   create_file();
}

