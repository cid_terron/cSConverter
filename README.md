This project provides an "easy-to-use" library for making unicode convertions.
Currently it offers a c++ library for converting between UTF8, UTF16 and UTF32.
It currently also supports ISO8859-1 to ISO8859-16
It shall be easy to use, free and easy to expand.
The name of the project stands for "C" string converter