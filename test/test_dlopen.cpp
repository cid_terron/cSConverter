/*
 * test_dlopen.cpp
 *
 *  Created on: Mar 28, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "cSConverter/cSConverter.h"
#include "cSConverter/UTF8.h"
#include "cSConverter/UTF32.h"

#include "stdio.h"
#include "string.h"


int main(int argc, char **argv)
{
   cSConverter::UTF8 *utf8 = (UTF8 *)cSConverterByName("SINGLE", "UTF-8");
   cSConverter::UTF32 *utf32 = (UTF32 *)cSConverterByName("BIG", "UTF-32");
   FILE *fp = fopen("DL_UTF32.txt", "w");
   if(utf8 && utf32)
   {
      *utf8 = "Hallo Welt";
      *utf32 = *utf8;
      *utf32 += *utf8;
      if(*utf32 == *utf8)
      {
         printf("Equals\n");
      }
      fwrite(utf32->get_string(), 1, utf32->get_string_size(), fp);
      fclose(fp);
   }
   else
   {
      printf("Failed to load UTF8\n");
   }

   return 0;
}


