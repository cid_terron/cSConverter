/*
 * test.c
 *
 *  Created on: Mar 21, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "cSConverter/cSConverter.h"

#include "stdio.h"
#include "string.h"


int main(int argc, char **argv)
{
   Converter_in_if *utf8 = cSConverterByName("SINGLE", "UTF-8");
   Converter_in_if *utf32 = cSConverterByName("BIG", "UTF-32");
   FILE *fp = fopen("utf32_be.txt", "w");
   if(utf8 && utf32 && fp)
   {
      const char *txt;
      size_t size;
      cSConverter_set_text(utf8, "Hallo\0", strlen("Hallo\n"));
      cSConverter_assign_utf(utf32, utf8);
      cSConverter_append_utf(utf32, utf32);
      cSConverter_get_content(&txt, &size, utf32);
      if(compare(utf32, utf8))
      {
         printf("Compare_ok\n");
      }
      fwrite(txt, 1, size, fp);
      fclose(fp);
      cSConverterFree(utf8);
   }
   else
   {
      printf("UTF8 does not exist\n");
   }
}


