/*
 * test.cpp
 *
 *  Created on: Feb 20, 2016
 *      Author: Dominik Frizel
 */
/*Copyright (c) 2016 Dominik Frizel

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "cSConverter/UTF8.h"
#include "cSConverter/UTF16.h"
#include "cSConverter/UTF32.h"
#ifdef CREATE_ISO8859
#include "cSConverter/ISO8859-1.h"
#endif
#include "stdio.h"


void test1()
{
   cSConverter::UTF8 utf;
     cSConverter::UTF16 utf2(SCONV_E_LITTLE);
     cSConverter::UTF16 utf2B(SCONV_E_BIG);
     cSConverter::UTF16 utf2BC(SCONV_E_BIG);
     cSConverter::UTF32 utf32B(SCONV_E_BIG);
     cSConverter::UTF32 utf32B2(SCONV_E_BIG);
     cSConverter::UTF32 utf32LC(SCONV_E_LITTLE);
     cSConverter::UTF32 utf32tmp(SCONV_E_LITTLE);
   #ifdef CREATE_ISO8859
     cSConverter::ISO8859_1 iso_1;
     cSConverter::ISO8859_1 iso_1_utf;
   #endif

     uint32_t c = 0;
     const char tmp[] = "Hallo Österreich\nBöse Welt\0";

     FILE *utf8 = fopen("out8.txt", "w");
     FILE *utf16_little = fopen("out16LE.txt", "w");
     FILE *utf16_big = fopen("out16BE.txt", "w");
     FILE *utf16_big_tmp = fopen("out16BEc_tmp.txt", "w");
     FILE *utf16_big_copy = fopen("out16BEc.txt", "w");
     FILE *utf32_big = fopen("out32BE.txt", "w");
     FILE *utf32_big2 = fopen("out32BE2.txt", "w");
     FILE *utf32_le = fopen("out32LE.txt", "w");
     FILE *utf32_tmp = fopen("out32_tmp.txt", "w");
     FILE *iso8859_1 = fopen("out8859-1.txt", "w");
     FILE *iso8859_1_utf = fopen("out8859-1_2utf.txt", "w");



     utf.add_cstring(tmp);
     utf.begin_parse(0);

     utf32tmp = tmp;

   #ifdef CREATE_ISO8859
     iso_1 = utf;
     iso_1 += utf32tmp;

     iso_1_utf = iso_1;
     iso_1_utf += iso_1;
   #endif

     utf2B = utf;
     utf2 = utf;
     utf2BC = utf2;
     fwrite(utf2BC.get_string(), 1, utf2BC.get_string_size(), utf16_big_tmp);
     utf32B = utf;
     utf32B2 = utf32B;

     utf32LC = utf32B;

   //  utf32B += utf32B;
   //  utf32B += utf32B;
     fwrite(utf32B.get_string(), 1, utf32B.get_string_size(), utf32_big);
     utf32B += utf2B;
     utf32B += utf;
     utf32LC += utf32B2;

     utf2BC += utf;

     utf += utf32LC;

     fwrite(utf2.get_string(), 1, utf2.get_string_size(), utf16_little);
     fwrite(utf2B.get_string(), 1, utf2B.get_string_size(), utf16_big);
     fwrite(utf2BC.get_string(), 1, utf2BC.get_string_size(), utf16_big_copy);
     fwrite(utf32B.get_string(), 1, utf32B.get_string_size(), utf32_big2);
     fwrite(utf32LC.get_string(), 1, utf32LC.get_string_size(), utf32_le);
     fwrite(utf.get_string(), 1, utf.get_string_size(), utf8);
     fwrite(utf32tmp.get_string(), 1, utf32tmp.get_string_size(), utf32_tmp);
   #ifdef CREATE_ISO8859
     fwrite(iso_1.get_string(), 1, iso_1.get_string_size(), iso8859_1);
     fwrite(iso_1_utf.get_string(), 1, iso_1_utf.get_string_size(), iso8859_1_utf);
   #endif

     fclose(utf32_big2);
     fclose(utf16_little);
     fclose(utf16_big);
     fclose(utf16_big_copy);
     fclose(utf32_big);
     fclose(utf32_le);
     fclose(utf8);
     fclose(utf32_tmp);
     fclose(utf16_big_tmp);
     fclose(iso8859_1);
     fclose(iso8859_1_utf);
}

void test2()
{
   char txt[5] = {'T', 'E', 'S', 'T', '\0'};
   char txt2[5] = {'T', 'E', 'S', 'T', '\0'};
   const char *tmp = txt;
   cSConverter::UTF8 utf, utf2;
   uint32_t U = 0;

//   while(tmp[0] != 0 && tmp != 0)
   while(1)
   {
      U = utf.get_next(&tmp);
      utf.add_unicode(U);
      if(U == 0)
      {
         break;
      }
   }
   utf.complete_add();
   utf2 = utf;
   printf("%s %i %s %i\n", utf.get_string(), utf.get_string_size(), utf2.get_string(), utf2.get_string_size());
   tmp = txt2;
   while(1)
   {
      U = utf.get_next(&tmp);
      utf2.add_unicode(U);
      if(U == 0)
      {
         break;
      }
   }

   utf2.complete_add();
   printf("%s %i %s %i\n", utf.get_string(), utf.get_string_size(), utf2.get_string(), utf2.get_string_size());

   utf2 += utf;
   printf("%s %i %s %i\n", utf.get_string(), utf.get_string_size(), utf2.get_string(), utf2.get_string_size());
}

int main(int argc, char **argv)
{
   test2();
}


